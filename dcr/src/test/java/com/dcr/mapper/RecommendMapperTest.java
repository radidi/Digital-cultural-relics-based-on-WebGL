package com.dcr.mapper;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class RecommendMapperTest {
    @Autowired
    private RecommendMapper recommendMapper;

    @Test
    void updateRelicCollectionNum() {
        System.out.println(recommendMapper.updateRelicCollectionNum(3,"文章"));
    }

    @Test
    void reduceCollectionNum() {
        System.out.println(recommendMapper.reduceCollectionNum(1,"文章"));
    }

    @Test
    void updateViewsNum() {
        System.out.println(recommendMapper.updateViewsNum(2,"藏品"));
    }
}