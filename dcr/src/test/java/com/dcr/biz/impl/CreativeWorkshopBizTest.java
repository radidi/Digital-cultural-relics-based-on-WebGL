package com.dcr.biz.impl;

import com.dcr.entity.DiyCreation;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CreativeWorkshopBizTest {

    @Autowired
    private CreativeWorkshopBiz creativeWorkshopBiz;
    @Test
    void addshow() {
        DiyCreation diyCreation = new DiyCreation();
        diyCreation.setName("ceshi");
        diyCreation.setPhoto("ceshi");
        diyCreation.setRelicId(1);
        diyCreation.setShowModelPath("ceshi");
        diyCreation.setUserId(1);
        int test = creativeWorkshopBiz.addshow(diyCreation);
        if(test!=0) System.out.println("ok");
    }
}