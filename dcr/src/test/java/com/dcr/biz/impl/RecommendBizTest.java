package com.dcr.biz.impl;

import com.dcr.entity.HomeRecommend;
import com.dcr.entity.Recommend;
import com.dcr.entity.Relic;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class RecommendBizTest {

    @Autowired
    private RecommendBiz recommendBiz;
    @Test
    void updateRelicRecommendStatus() {
        System.out.println(recommendBiz.updateRelicRecommendStatus(1,"文章","0"));
    }

    @Test
    void queryRecommend() {
        List<Recommend> recommends = recommendBiz.queryRecommend();
        for(Recommend recommend:recommends){
            System.out.println(recommend.toString());
        }
        //from dcr.recommend left join dcr.essay on (recommend.id = essay.essay_id and recommend.type='文章')left join dcr.relic on (recommend.id=relic.relic_id and recommend.type='藏品') limit 0,15;
    }

    @Test
    void queryRelicRecommend() {
        List<Relic> relics = recommendBiz.queryRelicRecommend();
        for(Relic relic:relics){
            System.out.println(relic.toString());
        }
    }

    @Test
    void queryByKeyword() {
        List<Recommend> list = recommendBiz.queryByKeyword("p");
        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }
    }
}