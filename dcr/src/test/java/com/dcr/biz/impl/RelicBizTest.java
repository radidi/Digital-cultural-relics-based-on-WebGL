package com.dcr.biz.impl;

import com.dcr.entity.Relic;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class RelicBizTest {
    @Autowired
    private RelicBiz relicBiz;

    @Test
    void queryRelic() {
        List<Relic> relics = relicBiz.queryRelic("","","");
        for(Relic relic:relics){
            System.out.println(relic.toString());
        }
        System.out.println("ok");
    }

    @Test
    void addRelic() {
        Relic relic = new Relic();
        relic.setIntroduce("高6.4厘米，口径4.9厘米，腹径7.5厘米。\n" +
                "\n" +
                "罐由白玉制成，圆口，鼓腹，圈足，足壁 镂雕3个方形孔。器表等距饰有3天鸡，作展翅直立状， 圆雕头部，翅膀与爪 浮雕于器身，以阴线刻出羽毛纹理，线条简洁流畅。\n" +
                "\n" +
                "清代的天鸡外形特点为前有卷须，后有垂凤尾。此器上的天鸡图案造型还保留着较为原始的形象，除双翅着意刻画外，凤尾、卷须尚未出现。如此形状的唐代天鸡作品非常少见。");
        relic.setRelicName("白玉天鸡三耳罐");
        relic.setRelicTime("唐");
        relic.setRelicType("玉器");
        relic.setModel(null);
        relic.setRelicPicture(null);
        int test = relicBiz.addRelic(relic);
        if(test!=0) System.out.println("ok");
    }

    @Test
    void updateRelic() {
        //int test = relicBiz.updateRelic("pop子RK800",2,"唐","青铜器",null,null,"唐代仿生pop子！RK800型号！");
    }

    @Test
    void showRelativeRelic() {
        List<Relic> relics = relicBiz.showRelativeRelic(3);
        for(Relic relic:relics){
            System.out.println(relic.toString());
        }
        System.out.println("ok");
    }

    @Test
    void showRelic() {
        Relic relic=relicBiz.showRelic(1);
        System.out.println(relic.toString());
    }
}