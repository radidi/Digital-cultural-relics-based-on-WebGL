package com.dcr.biz.impl;

import com.dcr.entity.PersonalCollectionDiy;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class PersonalCollectionDiyBizTest {
    @Autowired
    private PersonalCollectionDiyBiz personalCollectionDiyBiz;
    @Test
    void showCollectionList() {
        System.out.println(personalCollectionDiyBiz.showCollectionList(1));
    }

    @Test
    void showSelectedCollection() {
        System.out.println(personalCollectionDiyBiz.showSelectedCollection(3,1));
    }

    @Test
    void sumCollection() {
        System.out.println(personalCollectionDiyBiz.sumCollection(3));
    }

    @Test
    void collect() {
        PersonalCollectionDiy PCD = new PersonalCollectionDiy();
        PCD.setUserId(4);
        PCD.setShowId(1);
        personalCollectionDiyBiz.collect(PCD);
    }

    @Test
    void deleteCollection() {
        personalCollectionDiyBiz.deleteCollection(1,1);
    }

    @Test
    void queryCollections() {
        Date endTime = new Timestamp(System.currentTimeMillis());
        List<PersonalCollectionDiy> list = personalCollectionDiyBiz.queryCollections(1,null,"2020-01-01",endTime.toString());
        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }
        System.out.println("--------------------------------------------------------------");
    }

    @Test
    void whetherCollected() {
        personalCollectionDiyBiz.whetherCollected(1,1);
    }
}