package com.dcr.biz.impl;

import com.dcr.entity.News;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class NewsBizTest {
    @Autowired
    private NewsBiz newsBiz;
    @Test
    void showSentNews() {
        List<News> list= newsBiz.showSentNews(1);
        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }
    }

    @Test
    void showReceivedNews() {
        List<News> list= newsBiz.showReceivedNews(1);
        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }
    }

    @Test
    void showReceivedAdminNews() {
        List<News> list= newsBiz.showReceivedAdminNews(1);
        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }
    }

    @Test
    void showReceivedUserNews() {
        List<News> list= newsBiz.showReceivedUserNews(2);
        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }
    }

    @Test
    void sumSentNews() {
        System.out.println(newsBiz.sumSentNews(1));
    }

    @Test
    void sumReceivedNews() {
        System.out.println(newsBiz.sumReceivedNews(1));
    }

    @Test
    void showNewsDetail() {
        News news=newsBiz.queryByNewsId(7);
//        newsBiz.showNewsDetail(news);
        System.out.println(newsBiz.showNewsDetail(news));

    }

    @Test
    void updateNewsStatus() {
        newsBiz.updateNewsStatus(6);
    }

    @Test
    void queryByNewsId() {
    }

    @Test
    void sumUnreadAdminNews() {
        System.out.println(newsBiz.sumUnreadAdminNews(4));
    }

    @Test
    void sumUnreadUserNews() {
        System.out.println(newsBiz.sumUnreadUserNews(1));
    }

    @Test
    void showUnreadAdminNews() {
        List<News> list= newsBiz.showUnreadAdminNews(1);
        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }
    }

    @Test
    void showUnreadUserNews() {
        List<News> list= newsBiz.showUnreadUserNews(2);
        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }
    }

    @Test
    void sumReadAdminNews() {
        System.out.println(newsBiz.sumReadAdminNews(1));
    }

    @Test
    void sumReadUserNews() {
        System.out.println(newsBiz.sumReadUserNews(2));
    }

    @Test
    void showReadAdminNews() {
        List<News> list= newsBiz.showReadAdminNews(1);
        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }
    }

    @Test
    void showReadUserNews() {
        List<News> list= newsBiz.showReadUserNews(2);
        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }
    }
}