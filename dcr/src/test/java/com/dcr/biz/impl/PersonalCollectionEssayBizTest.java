package com.dcr.biz.impl;

import com.dcr.biz.IPersonalCollectionEssayBiz;
import com.dcr.entity.PersonalCollectionEssay;
import com.dcr.entity.PersonalCollectionRelic;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class PersonalCollectionEssayBizTest {

    @Autowired
    private PersonalCollectionEssayBiz personalCollectionEssayBiz;
    @Test
    void showCollectionList() {
        System.out.println(personalCollectionEssayBiz.showCollectionList(1));
    }

    @Test
    void showSelectedCollection() {
        System.out.println(personalCollectionEssayBiz.showSelectedCollection(1,1));
    }

    @Test
    void sumCollection() {
        System.out.println(personalCollectionEssayBiz.sumCollection(1));
    }

    @Test
    void collect() {
        PersonalCollectionEssay PCE = new PersonalCollectionEssay();
        PCE.setUserId(1);
        PCE.setEssayId(1);
        personalCollectionEssayBiz.collect(PCE);
    }

    @Test
    void deleteCollection() {
        personalCollectionEssayBiz.deleteCollection(1,1);
    }

    @Test
    void queryCollections() {
        List<PersonalCollectionEssay> list = personalCollectionEssayBiz.queryCollections(2,null,null,null);
        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }
        System.out.println("--------------------------------------------------------------");
    }

    @Test
    void deleteCollectionBatch() {
        personalCollectionEssayBiz.deleteCollectionBatch(3);
    }
}