package com.dcr.biz.impl;

import com.dcr.entity.Essay;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class EssayBizTest {
    @Autowired
    private EssayBiz essayBiz;
    @Test
    void showRelativeEassy() {
        int relicId = 1;
        List<Essay> essays = essayBiz.showRelativeEassy(relicId);
        for(Essay essay:essays){
            System.out.println(essay.toString());
        }
        System.out.println("ok");
    }

    @Test
    void queryEssayById() {
        Essay essay = essayBiz.queryEssayById(1);
        System.out.println(essay.toString());
    }

    @Test
    void insertEssay() {
        Essay essay = new Essay();
        essay.setTitle("ceshi");
        essay.setAuthor("特别版pop子");
        essay.setContent("pop子大降价");
        essay.setRelicId(2);
        essay.setUserId(1);
        int test = essayBiz.insertEssay(essay);
        System.out.println(test);
    }

    @Test
    void updateEssay() {
        Essay essay = new Essay();
        essay.setTitle("pop子新春特惠");
        essay.setContent("特别版pop子大降价");
        essay.setEssayId(2);
        essay.setRelicId(1);
        int test = essayBiz.updateEssay(essay);
    }

    @Test
    void deleteEassy() {
        int test = essayBiz.deleteEassy(4);
    }

    @Test
    void queryEssayByUserId() {
        List<Essay> essays = essayBiz.queryEssayByUserId(1);
        for(Essay essay:essays){
            System.out.println(essay.toString());
        }
        System.out.println("ok");
    }
}