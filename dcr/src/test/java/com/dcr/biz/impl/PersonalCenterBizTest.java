package com.dcr.biz.impl;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class PersonalCenterBizTest {

    @Autowired
    private PersonalCenterBiz personalCenterBiz;
    @Test
    void login() {
        System.out.println(personalCenterBiz.login("user","123456"));

    }

    @Test
    void regist() {
    }

    @Test
    void showData() {

        System.out.println(personalCenterBiz.showData(1));
    }

    @Test
    void updatePassword() {
        int userId=1;
        String newPass="oasisNo.2";
        personalCenterBiz.updatePassword(userId,newPass);
    }

    @Test
    void editPasswordByOld() {
        personalCenterBiz.editPasswordByOld(1,"oasisNo.2","blurNo.1");
    }
}