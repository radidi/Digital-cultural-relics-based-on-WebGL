package com.dcr.biz.impl;

import com.dcr.entity.HomeRecommend;
import com.dcr.entity.PersonalCollectionDiy;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class HomeRecommendBizTest {

    @Autowired
    private HomeRecommendBiz biz;

    @Test
    void show(){
        System.out.println(biz.show());
    }

    @Test
    void queryAll() {
        List<HomeRecommend> list = biz.queryAll();
        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }
    }


    @Test
    void queryByMaster() {
        System.out.println(biz.queryByMaster("","p"));
    }
}