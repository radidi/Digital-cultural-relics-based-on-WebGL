package com.dcr.biz.impl;

import com.dcr.entity.RelicComment;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class RelicCommentBizTest {
    @Autowired
    private RelicCommentBiz relicCommentBiz;

    @Test
    void testAddComment() {
        RelicComment relicComment=new RelicComment();
        relicComment.setContent("你俩有病吧，一边的粉还能互掐，sb");
        relicComment.setRelicId(1);
        relicComment.setUserId(3);
        relicComment.setReplyId(1);
        int test = relicCommentBiz.addComment(relicComment);
        if(test!=0) System.out.println("ok");
    }

    @Test
    void giveALike() {
        int test = relicCommentBiz.giveALike(1);
    }

//    @Test
//    void queryComment() {
//        List<RelicComment> relicComments = relicCommentBiz.queryComment(0);
//        for(RelicComment relicComment:relicComments){
//            System.out.println(relicComment.toString());
//        }
//    }

    @Test
    void deleteRelicComment() {
        int test = relicCommentBiz.deleteRelicComment(1);
        System.out.println(test);
    }

    @Test
    void queryUserIdByReplyId() {
        System.out.println(relicCommentBiz.queryUserIdByReplyId(1));
    }
}