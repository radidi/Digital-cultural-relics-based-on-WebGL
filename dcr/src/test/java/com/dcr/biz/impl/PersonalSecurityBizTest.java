package com.dcr.biz.impl;

import com.dcr.entity.PersonalSecurity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class PersonalSecurityBizTest {

    @Autowired
    private PersonalSecurityBiz personalSecurityBiz;
    @Test
    void addQuestion() {
        int questionId = 1;
        int userId=1;
        if(personalSecurityBiz.queryByUserId(userId).size() != 0)
            questionId = personalSecurityBiz.getMaxQuestionId(userId) + 1;
        String question="罗普兰是女的吗？";
        String answer="全世界没有女的了罗普兰也不是女的";
        System.out.println(questionId);
        personalSecurityBiz.addQuestion(userId,questionId,question,answer);
    }

    @Test
    void editQuestion() {
        int questionId = 1;
        int userId=2;
        String question="亨利卡维尔是男的吗?";
        String answer="铁血真男人，谢谢！";
        personalSecurityBiz.editQuestion(userId,questionId,question,answer);
    }

    @Test
    void deleteQuestion() {
        int questionId = 2;
        int userId=1;

        personalSecurityBiz.deleteQuestion(userId,questionId);
    }

    @Test
    void findPassword() {
        String username="user";
    }

    @Test
    void getAllQuestions() {
        List<PersonalSecurity> list = personalSecurityBiz.getAllQuestions("user");
        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }
    }

//    @Test
//    void getAllQuestionsTest() {
//        List<Map<String, String>> list=personalSecurityBiz.getAllQuestionsTest("user");
//        for(int i=0;i<list.size();i++){
//            System.out.println(list.get(i));
//        }
//
//    }
}