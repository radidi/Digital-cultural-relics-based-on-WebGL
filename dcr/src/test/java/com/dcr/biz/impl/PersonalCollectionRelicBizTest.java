package com.dcr.biz.impl;

import com.dcr.entity.PersonalCollectionRelic;
import com.dcr.entity.PersonalSecurity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@SpringBootTest
class PersonalCollectionRelicBizTest {
    @Autowired
    private PersonalCollectionRelicBiz personalCollectionBiz;
    @Test
    void showCollection() {
    }

    @Test
    void sumCollection() {
    }

    @Test
    void collect() {
        PersonalCollectionRelic pClc = new PersonalCollectionRelic();
        pClc.setUserId(4);
        pClc.setRelicId(1);
        personalCollectionBiz.collect(pClc);
    }

    @Test
    void deleteCollection() {
        personalCollectionBiz.deleteCollection(1,1);
    }

    @Test
    void queryCollections() throws ParseException {
        List<PersonalCollectionRelic> list = personalCollectionBiz.queryCollections(1,"唐",null,null,null,null);
        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }
        System.out.println("--------------------------------------------------------------");
        Date endTime = new Timestamp(System.currentTimeMillis());
        System.out.println("0"+endTime);
        Date startTime = null;
        System.out.println("1"+startTime);


        List<PersonalCollectionRelic> list2 = personalCollectionBiz.queryCollections(1,"p","2020-01-01",endTime.toString(),null,null);
        System.out.println("3："+list2.size());
        for(int i=0;i<list2.size();i++){

            System.out.println(list2.get(i));
        }
        System.out.println("--------------------------------------------------------------");
        List<PersonalCollectionRelic> list3= personalCollectionBiz.queryCollections(1,"p",null,null,"青铜器",null);
        System.out.println("4： "+list3.size());
        for(int i=0;i<list3.size();i++){

            System.out.println(list3.get(i));
        }
        System.out.println("--------------------------------------------------------------");
        List<PersonalCollectionRelic> list4= personalCollectionBiz.queryCollections(1,"p",null,null,"青铜器","唐");
        System.out.println("5： "+list4.size());
        for(int i=0;i<list4.size();i++){

            System.out.println(list4.get(i));
        }

    }

    @Test
    void deleteCollectionBatch() {
        personalCollectionBiz.deleteCollectionBatch(2);
    }
}