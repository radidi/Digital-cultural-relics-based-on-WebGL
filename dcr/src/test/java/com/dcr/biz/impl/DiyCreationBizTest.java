package com.dcr.biz.impl;

import com.dcr.entity.DiyCreation;
import com.dcr.entity.News;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class DiyCreationBizTest {
    @Autowired
    private DiyCreationBiz diyCreationBiz;

    @Test
    void showCreation() {
        List<DiyCreation> list= diyCreationBiz.showPersonalCreation(1);
        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }

    }

    @Test
    void showSelectedCreation() {
        System.out.println(diyCreationBiz.showSelectedCreation(1));
    }

    @Test
    void sumCreation() {
    }
}