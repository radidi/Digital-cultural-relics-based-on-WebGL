package com.dcr.biz.impl;

import com.dcr.entity.RelicCommentWhole;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class RelicCommentWholeBizTest {
    @Autowired
    private RelicCommentWholeBiz relicCommentWholeBiz;

    @Test
    void queryCommentWhole() {
        List<RelicCommentWhole> relicCommentWholes = relicCommentWholeBiz.queryCommentWhole(1);
        for(RelicCommentWhole relicCommentWhole:relicCommentWholes){
            System.out.println(relicCommentWhole.toString());
        }
    }
}