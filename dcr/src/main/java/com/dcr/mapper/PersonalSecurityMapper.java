package com.dcr.mapper;

import com.dcr.entity.PersonalSecurity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface PersonalSecurityMapper {

    List<PersonalSecurity> queryByUserId(int userId);

    int getMaxQuestionId(int userId);

    void addQuestion(PersonalSecurity ps);

    void editQuestion(PersonalSecurity ps);

    void deleteQuestion(int userId, int questionId);

    List<PersonalSecurity> getAllQuestions(String username);

//    List<Map<String, String>> getAllQuestions(String username);
}
