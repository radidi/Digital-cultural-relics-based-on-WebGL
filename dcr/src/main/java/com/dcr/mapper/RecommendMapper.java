package com.dcr.mapper;

import com.dcr.entity.Recommend;
import com.dcr.entity.Relic;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface RecommendMapper {
    //修改推荐是否显示
    int updateRelicRecommendStatus(int id,String type,String status);
    //收藏数+1，并更新recommend表热度
    int updateRelicCollectionNum(int id,String type);
    //收藏数-1，并更新recommend表热度
    int reduceCollectionNum(int id,String type);
    //查前15推荐
    List<Recommend> queryRecommend();
    //藏品浏览量+1,并更新recommend表热度
    int updateViewsNum(int id,String type);
    //查看藏品推荐(前8)
    List<Relic> queryRelicRecommend();
    //管理员
    //管理员查看藏品推荐（30）
    List<Relic> MqueryRelicRecommend();
    //管理员查看推荐（30）
    List<Recommend> MqueryRecommend();

    //删除推荐
    int deleteRecommend(int id,String type);
    //管理员查看某类推荐（30）
    List<Recommend> MqueryRecommendByType(String type);

    //客户根据关键词搜索，还没完成T T
    List<Recommend> queryByKeyword(String keyword);
}
