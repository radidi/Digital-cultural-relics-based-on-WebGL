package com.dcr.mapper;

import com.dcr.entity.PersonalCollectionDiy;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PersonalCollectionDiyMapper {
    int sumCollection(int userId);


    void collect(PersonalCollectionDiy PCD);

    List<PersonalCollectionDiy> showCollectionList(int userId);

    void deleteCollection(int userId, int showId);

    PersonalCollectionDiy showSelectedCollection(int userId, int showId);

    List<PersonalCollectionDiy> queryCollections(int userId, String keyword, String startTime, String endTime);

    int deleteCollectionBatch(int showId);

    Integer whetherCollected(int showId, int userId);

    int deleteBatchByRelicId(int relicId);
}
