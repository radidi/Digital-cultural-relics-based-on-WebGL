package com.dcr.mapper;

import com.dcr.entity.PersonalCollectionEssay;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PersonalCollectionEssayMapper {
    int sumCollection(int userId);


    void collect(PersonalCollectionEssay PCE);

    List<PersonalCollectionEssay> showCollectionList(int userId);

    void deleteCollection(int userId, int essayId);

    PersonalCollectionEssay showSelectedCollection(int userId, int essayId);

    List<PersonalCollectionEssay> queryCollections(int userId, String keyword, String startTime, String endTime);

    int deleteCollectionBatch(int essayId);

    Integer whetherCollected(int essayId, int userId);
}
