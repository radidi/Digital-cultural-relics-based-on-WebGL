package com.dcr.mapper;

import com.dcr.entity.DiyCreation;
import com.dcr.entity.DiyCreationSearch;
import com.dcr.entity.PersonalCollectionDiy;
import com.dcr.entity.PersonalCollectionRelic;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DiyCreationMapper {

    //dy
    int sumCreation(int userId);

    DiyCreation showSelectedCreation(int showId);

    List<DiyCreation> showPersonalCreation(int userId);
    //lwj
    int deletework(int showId);

    List<DiyCreationSearch>  findwork( DiyCreationSearch diyCreationSearch);

    int updateCollectionNum(int showId);

    int addDiyWork(PersonalCollectionDiy personalCollectionDiy);

    DiyCreation findDiyWorkById(int showId);

    int updateCreation(DiyCreation diyCreation);

    //lsy
    //按relicid删除diy
    int deleteDiyWorkByRelicId(int relicId);
    //按relicid查询showid的List
    int[] queryShowIdByRlicId(int relicId);
}
