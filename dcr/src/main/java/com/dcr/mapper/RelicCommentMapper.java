package com.dcr.mapper;

import com.dcr.entity.RelicComment;
import org.apache.ibatis.annotations.Mapper;

import java.sql.Timestamp;
import java.util.List;

@Mapper
public interface RelicCommentMapper {
    //添加评论
    //必须有int commentId, int relicId, int userId, String content,int replyId, Timestamp time
    int addComment(RelicComment relicComment);
    //求最大id
    int maxRelicCommentId();
    //求藏品数量
    int countRelicCommentNum();

    //评论点赞
    int giveALike(int commentId);
    //查询某文物根评论
    List<RelicComment> queryCommentByRelic(int relicId);
    //查询回复列表
    List<RelicComment> queryComment(int replyId);

    //删除回复评论
    int deleteRelicCommentReply(int commentId);
    //删除评论
    int deleteRelicComment(int commentId);
    //根据replyid查找userid
    int queryUserIdByReplyId(int replyId);
    //删除某文物评论
    int deleteRelicCommentByRelicId(int relicId);
}
