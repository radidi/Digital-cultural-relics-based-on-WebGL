package com.dcr.mapper;

import com.dcr.entity.HomeRecommend;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface HomeRecommendMapper {
    //显示所有被推荐的
    List<HomeRecommend> show();

    //管理员查看所有首页推荐表的内容
    List<HomeRecommend> queryAll();

    //管理员搜索
    List<HomeRecommend> queryByMaster(@Param("type") String type,@Param("key") String key);

    //管理员添加推荐
    int add(HomeRecommend homeRecommend);

    //管理员删除推荐
    int deleteById(int id);

    //管理员编辑推荐
    int editById(HomeRecommend homeRecommend);

    //根据使用次数升降序应该可以用前端完成。

    //获取当前最大id（有没有更好的办法，感觉这个好蠢
    int getMax();

    int openRecommend(HomeRecommend homeRecommend);

    int closeRecommend(HomeRecommend homeRecommend);
}
