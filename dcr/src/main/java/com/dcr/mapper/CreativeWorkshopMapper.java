package com.dcr.mapper;

import com.dcr.entity.CreativeWorkshopSearch;
import com.dcr.entity.DiyCreation;
import com.dcr.entity.PersonalCollectionRelic;
import com.dcr.entity.Relic;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Mapper
public interface CreativeWorkshopMapper {

     List<Relic> findwork(CreativeWorkshopSearch creativeWorkshopSearch);
    int addshow(@RequestBody DiyCreation diyCreation);
    int addcollection( PersonalCollectionRelic personalCollectionRelic);

    //以下是lsy的地盘
    int maxEssayId();
    int countDiyNum();
}
