package com.dcr.mapper;

import com.dcr.entity.PersonalCenter;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PersonalCenterMapper {
    PersonalCenter login(PersonalCenter pc);

    int register(PersonalCenter pc);

    void updatePassword(PersonalCenter pc);

    PersonalCenter getPersonalInfoById(int userId);

    void editPasswordByOld(PersonalCenter pc);

    void changeInformation(PersonalCenter pc);

    PersonalCenter queryUserByUsername(String username);

    List<Integer> getAllUsersId();

    List<PersonalCenter> getAllOrdinaryUsers();

//    int sumCreation(int userId);
}
