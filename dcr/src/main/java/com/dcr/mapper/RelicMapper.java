package com.dcr.mapper;

import com.dcr.entity.Relic;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface RelicMapper {
    //搜索藏品
    //查某一类的所有藏品
    List<Relic> queryRelic(String relicName,String relicTime,String relicType);
    //查藏品详情
    Relic showRelic(int relicId);

    //添加藏品
    //必须有String relicName,int relicId,String relicTime,String type,String relicPicture,String model,String introduce
    int addRelic(Relic relic);
    //求最大id
    int maxRelicId();
    //求藏品数量
    int countRelicNum();

    //修改藏品
    //必须有String relicName,int relicId,String relicTime,String type,String relicPicture,String model,String introduce
    int updateRelic(Relic relic);
    //查藏品相关文物
    List<Relic> showRelativeRelic(int relicId);

    //删除藏品,仅删除藏品
    int deleteRelic(int relicId);
}
