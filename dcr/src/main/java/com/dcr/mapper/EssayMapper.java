package com.dcr.mapper;

import com.dcr.entity.Essay;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface EssayMapper {
    //查藏品相关文章
    List<Essay> showRelativeEassy(int relicId);
    //搜索指定文章
    Essay queryEssayById(int essayId);
    //上传文章
    //必须有int essayId,String title,String author,String content,int relicId
    int insertEssay(Essay essay);
    //求id最大值
    int maxEssayId();
    //求文章数量
    int countEssayNum();
    //编辑文章
    //必须有int essayId,String title,String content,int relicId
    int updateEssay(Essay essay);
    //删除文章
    int deleteEassy(int essayId);
    //按用户id查找文章
    List<Essay> queryEssayByUserId(int userId);
    //用标题搜索文章
    List<Essay> queryEssay(String title);
    //修改某文物关联文章relicId为0
    int updateEssayRelicId(int relicId);

}
