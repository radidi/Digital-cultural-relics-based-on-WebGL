package com.dcr.mapper;

import com.dcr.entity.PersonalCollectionRelic;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PersonalCollectionRelicMapper {
    int sumCollection(int userId);


    void collect(PersonalCollectionRelic pClc);

    List<PersonalCollectionRelic> showCollectionList(int userId);

    void deleteCollection(int userId, int relicId);

    PersonalCollectionRelic showSelectedCollection(int userId, int relicId);

    List<PersonalCollectionRelic> queryCollections(int userId, String keyword, String startTime, String endTime, String type, String time);

    int deleteCollectionBatch(int relicId);

    Integer whetherCollected(int relicId, int userId);
}
