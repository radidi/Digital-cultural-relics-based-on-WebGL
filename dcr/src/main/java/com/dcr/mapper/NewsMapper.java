package com.dcr.mapper;

import com.dcr.entity.News;
import com.dcr.entity.PersonalCenter;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface NewsMapper {
    List<News> showSentNews(int senderId);

    List<News> showReceivedNews(int recipientId);

    int sumSentNews(int senderId);

    int sumReceivedNews(int recipientId);

    List<News> showReceivedAdminNews(int recipientId);

    List<News> showReceivedUserNews(int recipientId);

    News showNewsDetail(News news);

//    String updateNewsStatus(News news);
//
    void updateNewsStatus(News news);

    News queryByNewsId(int newsId);

    int sumUnreadNews(int recipientId);

    int sumUnreadAdminNews( int recipientId);

    int sumUnreadUserNews(int recipientId);

    List<News> showUnreadAdminNews(int recipientId);

    List<News> showUnreadUserNews(int recipientId);

    int sumReadNews(int recipientId);

    int sumReadAdminNews(int recipientId);

    int sumReadUserNews(int recipientId);

    List<News> showReadAdminNews(int recipientId);

    List<News> showReadUserNews(int recipientId);

    int deleteSelectedNews(int newsId);

    int deleteBatchNews(List<Integer> ids);

    int sumReceivedAdminNews(int recipientId);

    int sumReceivedUserNews(int recipientId);

    int sendNews(News news);

    int masterSendNewsBatch(List<News> newsList);

    List<News> showHistory(int senderId, int recipientId);

    List<PersonalCenter> historyUsers(int senderId);

}
