package com.dcr.entity;

import lombok.Data;

@Data
public class HomeRecommend {

    private int id;
    private int originId;
    private String type;
    private int status;
    private int times;
    private String introduction;
    private String image;
    private String name;

    private Relic relic;
    private Essay essay;
    private DiyCreation diyCreation;

    public void setId(int id) {
        this.id = id;
    }

    public void setOriginId(int originId) {
        this.originId = originId;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setTimes(int times) {
        this.times = times;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRelic(Relic relic) {
        this.relic = relic;
    }

    public void setEssay(Essay essay) {
        this.essay = essay;
    }

    public void setDiyCreation(DiyCreation diyCreation) {
        this.diyCreation = diyCreation;
    }

}
