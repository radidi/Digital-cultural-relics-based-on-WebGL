package com.dcr.entity;

import lombok.Data;

@Data
public class PersonalSecurity {
    private int userId;
    private int questionId;
    private String question;
    private String answer;
    private PersonalCenter personalCenter;
}
