package com.dcr.entity;

import lombok.Data;

import java.util.List;

@Data
public class RelicCommentWhole {
    public RelicComment relicComment;
    public List<RelicComment> relicComments;
}
