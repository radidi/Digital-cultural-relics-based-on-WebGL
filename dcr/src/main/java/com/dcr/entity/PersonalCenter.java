package com.dcr.entity;

import lombok.Data;

@Data
public class PersonalCenter {
    private int userId;
    private String username;
    private String role;
    private int grade;
    private String password;
    private String photo;
    private String download;
}
