package com.dcr.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.sql.Timestamp;
import java.util.Date;

@Data
public class News {
    private int senderId;
    private int recipientId;
    private int newsId;
    private String content;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp time;
    private String status;
    private String type;
    private PersonalCenter personalCenter;

}
