package com.dcr.entity;

import lombok.Data;

/**
 * 工具实体
 * 专用的
 */
@Data
public class DiyCreationSearch {
    private String relicType;
    private String relicTime;
    private String search;

    private int showId;
    private int relicId;
    private int userId;
    private String showModelPath;
    private String photo;
    private String name;

    private String relicName;
    private String introduce;
    private int diyCollectionNum;
    private int diyViewsNum;
}
