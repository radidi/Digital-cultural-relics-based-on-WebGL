package com.dcr.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.sql.Timestamp;
import java.util.Date;

@Data
public class PersonalCollectionEssay {
    private int userId;
    private int essayId;
    private int showId;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp collectionTime;
    private Relic relic;

    private Essay essay;

}
