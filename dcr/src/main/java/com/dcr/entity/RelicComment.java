package com.dcr.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class RelicComment {
    private int commentId;
    private int relicId;
    private int userId;
    private int likesNum;
    private String content;
    private int replyId;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp time;

    //评论关联
    //private PersonalCenter personalCenter;
    private String username;
    private String photo;
}
