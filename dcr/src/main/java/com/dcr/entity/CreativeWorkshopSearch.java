package com.dcr.entity;

import lombok.Data;
/**
 * 工具实体
 * 专用的
 */
@Data
public class CreativeWorkshopSearch {
    private String relicType;
    private String relicTime;
    private String search;
}
