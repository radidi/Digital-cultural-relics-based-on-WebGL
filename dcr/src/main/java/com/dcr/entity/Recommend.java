package com.dcr.entity;

import lombok.Data;

@Data
public class Recommend {
    private int id;
    private int hotNum;
    private String status;
    private String type;

    //关联
    private Relic relic;
    private Essay essay;
    private DiyCreation diyCreation;
}
