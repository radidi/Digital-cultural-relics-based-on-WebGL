package com.dcr.entity;

import lombok.Data;

@Data
public class DiyCreation {
    private int showId;
    private int relicId;
    private int userId;
    private String showModelPath;
    private String photo;
    private String name;
    private int diyCollectionNum;
    private int diyViewsNum;

}
