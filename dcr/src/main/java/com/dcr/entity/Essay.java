package com.dcr.entity;

import lombok.Data;

@Data
public class Essay {
    private int essayId;
    private String title;
    private String author;
    private String content;
    private int relicId;
    private int essayCollectionNum;
    private int essayViewsNum;
    private int userId;

    private String photo;
}
