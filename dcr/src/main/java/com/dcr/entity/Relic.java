package com.dcr.entity;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("relic")//别名
public class Relic {
    private String relicName;
    private int relicId;
    private String relicTime;
    private String relicType;
    private String relicPicture;
    private String model;
    private String introduce;
    private int relicCollectionNum;
    private int relicViewsNum;
}
