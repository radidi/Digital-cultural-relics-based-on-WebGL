package com.dcr.controller;

import com.dcr.biz.impl.HomeRecommendBiz;
import com.dcr.entity.HomeRecommend;
import com.dcr.entity.Relic;
import com.dcr.utils.Result;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/AdminHomeRecommend")
@CrossOrigin
public class AdminHomeRecommendController {
    @Autowired
    private HomeRecommendBiz homeRecommendBiz;

    //管理员查所有首页推荐表内容
    @GetMapping(value = {"/queryAll"})
    public Result queryAll(){
        return Result.ok(homeRecommendBiz.queryAll());
    }


    //管理员搜索
    @GetMapping(value = {"/queryByMaster"})
    public Result queryByMaster(@RequestParam String type, @RequestParam String key){
        return Result.ok(homeRecommendBiz.queryByMaster(type,key));
    }

    //管理员添加推荐
    @PostMapping(value = {"/add"})
    public @ResponseBody
    Result<?> add(String introduction,String image,Integer status,String name,Integer originId,String type)
    {
        HomeRecommend homeRecommend=new HomeRecommend();
        homeRecommend.setImage(image);
        homeRecommend.setIntroduction(introduction);
        homeRecommend.setStatus(status);
        homeRecommend.setName(name);
        homeRecommend.setOriginId(originId);
        homeRecommend.setType(type);
        homeRecommend.setTimes(1);
        return Result.ok(homeRecommendBiz.add(homeRecommend));
    }

    //管理员开启推荐状态
    @PostMapping(value = {"/openRecommend"})
    public @ResponseBody
    Result<?> openRecommend(int id)
    {
        return Result.ok(homeRecommendBiz.openRecommend(id));
    }

    //关闭推荐状态
    @PostMapping(value = {"/closeRecommend"})
    public @ResponseBody
    Result<?> closeRecommend(int id)
    {
        return Result.ok(homeRecommendBiz.closeRecommend(id));
    }


    //编辑推荐
    @PostMapping(value = {"/editById"})
    public @ResponseBody
    Result<?> editById(int id, int status, String introduction,  String  image)
    {
        return Result.ok(homeRecommendBiz.editById(id, status, introduction, image));
    }

}
