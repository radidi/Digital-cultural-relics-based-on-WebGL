package com.dcr.controller;

import com.dcr.biz.IEssayBiz;
import com.dcr.entity.Essay;
import com.dcr.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/Essay")
@CrossOrigin
public class EssayController {
    @Autowired
    private IEssayBiz essayBiz;

    //搜索指定文章
    @RequestMapping(value = "/queryEssayById",method = RequestMethod.GET)
    public Result queryEssayById(int essayId){
        return Result.ok(essayBiz.queryEssayById(essayId));
    }
    //上传文章
    //需要title,author,content,relicId,userId
    @RequestMapping(value = "/insertEssay",method = RequestMethod.POST)
    public @ResponseBody
    int insertEssay(@RequestBody Essay essay){
        return essayBiz.insertEssay(essay);
    }
    //编辑文章
    @RequestMapping(value = "/updateEssay",method = RequestMethod.POST)
    public @ResponseBody
    int updateEssay(@RequestBody Essay essay){
        return essayBiz.updateEssay(essay);
    }
    //删除文章
    @RequestMapping(value = "/deleteEassy",method = RequestMethod.POST)
    public @ResponseBody
    int deleteEassy(@RequestBody Map<String,Integer> map){
        return essayBiz.deleteEassy(map.get("essayId"));
    }
    //文章浏览量+1，并更新recommend表热度
    @RequestMapping(value = "/updateEssayViewsNum",method = RequestMethod.POST)
    public @ResponseBody
    int updateEssayViewsNum(@RequestParam(required = true) int essayId){
        return essayBiz.updateEssayViewsNum(essayId);
    }
}
