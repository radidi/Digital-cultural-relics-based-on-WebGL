package com.dcr.controller;

import com.dcr.biz.*;
import com.dcr.entity.*;
import com.dcr.mapper.DiyCreationMapper;
import com.dcr.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.*;

@CrossOrigin
@Controller
@RequestMapping("/PersonalCenter")
public class PersonalCenterController {
    @Autowired
    private IPersonalCenterBiz personalCenterBiz;
    @Autowired
    private IDiyCreationBiz diyCreationBiz;
    @Autowired
    private INewsBiz newsBiz;
    @Autowired
    private IPersonalCollectionRelicBiz personalCollectionRelicBiz;
    @Autowired
    private IPersonalCollectionEssayBiz personalCollectionEssayBiz;
    @Autowired
    private IPersonalCollectionDiyBiz personalCollectionDiyBiz;
    @Autowired
    private IEssayBiz essayBiz;
//------------用户数据---------------------------------------------------------------------------------------------
    //显示用户数据
    @GetMapping(value = {"/showData"})
    public @ResponseBody
    Result showData(@RequestParam int userId){
        return Result.ok(personalCenterBiz.showData(userId));
    }

    //获得所有普通用户基本信息（头像、ID、用户名）
    @GetMapping(value = {"/getAllOrdinaryUsers"})
    public @ResponseBody
    Result getAllOrdinaryUsers(){
        return Result.ok(personalCenterBiz.getAllOrdinaryUsers());
    }


    //根据用户名查询用户信息
    @GetMapping(value = {"/queryUserByUsername"})
    public @ResponseBody
    Result queryUserByUsername(@RequestParam String username){
        return Result.ok(personalCenterBiz.queryUserByUsername(username));
    }

//-------------DIY作品-----------------------------------------------------------------------------------------------

    //显示我的创意工坊作品
    @GetMapping(value = {"/showPersonalCreation"})
    public @ResponseBody
    Result showPersonalCreation(@RequestParam int userId){
        List<DiyCreation> diys= diyCreationBiz.showPersonalCreation(userId);
        return Result.ok(diys);
    }

    //显示我的diy作品数量
    @GetMapping(value = {"/sumCreation"})
    public @ResponseBody
    Result sumCreation(@RequestParam int userId){
        int sumCreation=diyCreationBiz.sumCreation(userId);
        return Result.ok(sumCreation);
    }

    //显示我的某一个diy作品的具体信息
    @GetMapping(value = {"/showSelectedCreation"})
    public @ResponseBody
    Result showSelectedCreation(@RequestParam int showId){
        return Result.ok(diyCreationBiz.showSelectedCreation(showId));
    }

    //删除我的diy作品
    @PostMapping("/deletework")
    @ResponseBody
    public Result deletework(@RequestParam int userId,@RequestParam int showId){
        DiyCreation diyCreation= diyCreationBiz.findDiyWorkById(showId);
        if (diyCreation.getUserId()==userId){
            return Result.ok(diyCreationBiz.deletework(diyCreation.getShowId()));
        }else
            return Result.error("只能删除自己的作品");

    }

    //编辑我的diy作品
    @PostMapping("/updateCreation")
    @ResponseBody
    public Result updateCreation(@RequestBody Map<String, String> map){
        DiyCreation diyCreation=new DiyCreation();
        diyCreation.setShowId(Integer.parseInt(map.get("showId")));
        diyCreation.setUserId(Integer.parseInt(map.get("userId")));
        diyCreation.setName(map.get("name"));
        diyCreation.setPhoto(map.get("photo"));
        diyCreation.setShowModelPath(map.get("showModelPath"));
        return Result.ok(diyCreationBiz.updateCreation(diyCreation));
    }

    /**
     * 根据userId，进行模糊搜索
     * 就是在个人作品页面（已经显示了该用户的所有作品），进一步精确搜索，即模糊搜索某个用户的作品
     * 这里post没有改成get
     */
    @PostMapping("/fuzzySearchPersonalDiy")
    @ResponseBody
    public List<DiyCreationSearch> fuzzySearchPersonalDiy(@RequestBody DiyCreationSearch diyCreationSearch){
        return diyCreationBiz.findwork(diyCreationSearch);
    }

    /**
     *批量删除我的作品
     */
    @PostMapping("/deleteworks")
    @ResponseBody
    public int deleteworks(@RequestBody List<DiyCreation> list){
        for(int i=0;i<list.size();i++){
            DiyCreation diyCreation=new DiyCreation();
            diyCreation.setShowId(list.get(i).getShowId());
            diyCreation.setUserId(list.get(i).getUserId());
            diyCreationBiz.deletework(diyCreation.getShowId());
        }
        return 1;
    }
//-------------消息-----------------------------------------------------------------------------------------------

    //显示已发送消息列表
    @GetMapping(value = {"/showSentNews"})
    public @ResponseBody
    Result showSentNews(@RequestParam int userId){
        int senderId=userId;
        return Result.ok(newsBiz.showSentNews(senderId));
    }

    //显示已发送消息计数
    @GetMapping(value = {"/sumSentNews"})
    public @ResponseBody
    Result sumSentNews(@RequestParam int userId){
        int senderId=userId;
        return Result.ok(newsBiz.sumSentNews(senderId));
    }

    //显示全部接收消息
    @GetMapping(value = {"/showReceivedNews"})
    public @ResponseBody
    Result showReceivedNews(@RequestParam int userId){
        int recipientId=userId;
        return Result.ok(newsBiz.showReceivedNews(recipientId));
    }

    //收到的消息计数
    @GetMapping(value = {"/sumReceivedNews"})
    public @ResponseBody
    Result sumReceivedNews(@RequestParam int userId){
        int recipientId=userId;
        return Result.ok(newsBiz.sumReceivedNews(recipientId));
    }

    //显示接收到的管理员消息
    @GetMapping(value = {"/showReceivedAdminNews"})
    public @ResponseBody
    Result showReceivedAdminNews(@RequestParam int userId){
        int recipientId=userId;
        return Result.ok(newsBiz.showReceivedAdminNews(recipientId));
    }

    //显示接收到的管理员消息数
    @GetMapping(value = {"/sumReceivedAdminNews"})
    public @ResponseBody
    Result sumReceivedAdminNews(@RequestParam int userId){
        int recipientId=userId;
        return Result.ok(newsBiz.sumReceivedAdminNews(recipientId));
    }


    //显示接收到的用户消息
    @GetMapping(value = {"/showReceivedUserNews"})
    public @ResponseBody
    Result showReceivedUserNews(@RequestParam int userId){
        int recipientId=userId;
        return Result.ok(newsBiz.showReceivedUserNews(recipientId));
    }

    //显示接收到的用户消息数
    @GetMapping(value = {"/sumReceivedUserNews"})
    public @ResponseBody
    Result sumReceivedUserNews(@RequestParam int userId){
        int recipientId=userId;
        return Result.ok(newsBiz.sumReceivedUserNews(recipientId));
    }

    //未读消息计数
    @GetMapping(value = {"/sumUnreadNews"})
    public @ResponseBody
    Result sumUnreadNews(@RequestParam int userId){
        int recipientId=userId;
        return Result.ok(newsBiz.sumUnreadNews(recipientId));
    }

    //未读管理员消息计数
    @GetMapping(value = {"/sumUnreadAdminNews"})
    public @ResponseBody
    Result sumUnreadAdminNews(@RequestParam int userId){
        int recipientId=userId;
        return Result.ok(newsBiz.sumUnreadAdminNews(recipientId));
    }

    //未读用户消息计数
    @GetMapping(value = {"/sumUnreadUserNews"})
    public @ResponseBody
    Result sumUnreadUserNews(@RequestParam int userId){
        int recipientId=userId;
        return Result.ok(newsBiz.sumUnreadUserNews(recipientId));
    }
    //显示未读的管理员消息
    @GetMapping(value = {"/showUnreadAdminNews"})
    public @ResponseBody
    Result showUnreadAdminNews(@RequestParam int userId){
        int recipientId=userId;
        return Result.ok(newsBiz.showUnreadAdminNews(recipientId));
    }
    //显示未读的用户消息
    @GetMapping(value = {"/showUnreadUserNews"})
    public @ResponseBody
    Result showUnreadUserNews(@RequestParam int userId){
        int recipientId=userId;
        return Result.ok(newsBiz.showUnreadUserNews(recipientId));
    }

    //已读消息计数
    @GetMapping(value = {"/sumReadNews"})
    public @ResponseBody
    Result sumReadNews(@RequestParam int userId){
        int recipientId=userId;
        return Result.ok(newsBiz.sumReadNews(recipientId));
    }

    //已读管理员消息计数
    @GetMapping(value = {"/sumReadAdminNews"})
    public @ResponseBody
    Result sumReadAdminNews(@RequestParam int userId){
        int recipientId=userId;
        return Result.ok(newsBiz.sumReadAdminNews(recipientId));
    }

    //已读用户消息计数
    @GetMapping(value = {"/sumReadUserNews"})
    public @ResponseBody
    Result sumReadUserNews(@RequestParam int userId){
        int recipientId=userId;
        return Result.ok(newsBiz.sumReadUserNews(recipientId));
    }
    //显示已读的管理员消息
    @GetMapping(value = {"/showReadAdminNews"})
    public @ResponseBody
    Result showReadAdminNews(@RequestParam int userId){
        int recipientId=userId;
        return Result.ok(newsBiz.showReadAdminNews(recipientId));
    }
    //显示已读的用户消息
    @GetMapping(value = {"/showReadUserNews"})
    public @ResponseBody
    Result showReadUserNews(@RequestParam int userId){
        int recipientId=userId;
        return Result.ok(newsBiz.showReadUserNews(recipientId));
    }

    //显示具体消息(消息的具体内容+发送者的信息，并把消息状态从未读改为已读）
    @GetMapping(value = {"/showNewsDetail"})
    public @ResponseBody
    Result showNewsDetail(@RequestParam int newsId){
        News news=new News();
        news.setNewsId(newsId);
//        news.setRecipientId(Integer.parseInt(map.get("recipientId")));
        return Result.ok(newsBiz.showNewsDetail(news));
    }


    //删除选定消息
    @PostMapping(value = {"/deleteSelectedNews"})
    public @ResponseBody
    Result deleteSelectedNews(@RequestParam int newsId){
        return Result.ok(newsBiz.deleteSelectedNews(newsId));
    }
    //批量删除消息
    @PostMapping(value = {"/deleteBatchNews"})
    public @ResponseBody
    Result deleteBatchNews(@RequestBody List<Integer> list){
        for(int i=0;i<list.size();i++){
            newsBiz.deleteSelectedNews(list.get(i));

        }
        return Result.ok("success");
    }

    //批量删除消息2,试试foreach,成功了，不知道和上面的哪个更快
    @PostMapping(value = {"/deleteBatchNewsTest"})
    public @ResponseBody
    Result deleteBatchNewsTest(@RequestBody List<Integer> ids){
        return Result.ok(newsBiz.deleteBatchNews(ids));
    }

    //管理员给某个用户发送消息
    @PostMapping(value = {"/masterSendNews"})
    public @ResponseBody
    Result masterSendNews(@RequestBody Map<String,String> map){
        News news=new News();
        news.setRecipientId(Integer.parseInt(map.get("recipientId")));
        news.setSenderId(Integer.parseInt(map.get("senderId")));
        news.setContent(map.get("content"));
        news.setStatus("未读");
        news.setType("0");
        Timestamp releasedTime=new Timestamp(System.currentTimeMillis());
        news.setTime(releasedTime);
        return Result.ok(newsBiz.sendNews(news));
    }

//    //管理员批量发送消息，前端ids要写成字符串，如"ids":"4,5,6",爱用哪个用哪个，自己改名就行了
    @PostMapping(value = {"/masterSendNewsBatch"})
    public @ResponseBody
    Result masterSendNewsBatch(@RequestBody Map<String,String> map){
        String idString=map.get("ids");
        List<String> ids= Arrays.asList(idString.split(","));
        for(int i=0;i<ids.size();i++){
            News news=new News();
            news.setRecipientId(Integer.parseInt(ids.get(i)));
            news.setSenderId(Integer.parseInt(String.valueOf(map.get("senderId"))));
            news.setContent(String.valueOf(map.get("content")));
            news.setStatus("未读");
            Timestamp releasedTime=new Timestamp(System.currentTimeMillis());
            news.setTime(releasedTime);
            news.setType("0");
            newsBiz.sendNews(news);
        }

        return Result.ok("success");
    }

    //管理员批量发送消息,foreach版,成功了
    @PostMapping(value = {"/masterSendNewsBatchTest"})
    public @ResponseBody
    Result masterSendNewsBatchTest(@RequestParam int senderId,@RequestParam String content,@RequestParam List<Integer> ids){

        List<News> newsList = new ArrayList<>();
        for(int i=0;i<ids.size();i++){
            News news=new News();
            news.setRecipientId(ids.get(i));
            news.setSenderId(senderId);
            news.setContent(content);
            news.setStatus("未读");
            Timestamp releasedTime=new Timestamp(System.currentTimeMillis());
            news.setTime(releasedTime);
            news.setType("0");

            newsList.add(news);

        }

        return Result.ok(newsBiz.masterSendNewsBatch(newsList));
    }


    //管理员给所有人发送消息
    @PostMapping(value = {"/masterSendNewsToAll"})
    public @ResponseBody
    Result masterSendNewsToAll(@RequestBody Map<String,String> map){

        List<Integer> ids=personalCenterBiz.getAllUsersId();
        List<News> newsList = new ArrayList<>();
        for(int i=0;i<ids.size();i++){
            News news=new News();
            news.setRecipientId(ids.get(i));
            news.setSenderId(Integer.parseInt(map.get("senderId")));
            news.setContent(map.get("content"));
            news.setStatus("未读");
            Timestamp releasedTime=new Timestamp(System.currentTimeMillis());
            news.setTime(releasedTime);
            news.setType("1");
            newsList.add(news);

        }

        return Result.ok(newsBiz.masterSendNewsBatch(newsList));
    }

    //显示具体消息(消息的具体内容+发送者的信息，并把消息状态从未读改为已读）
    @GetMapping(value = {"/showHistory"})
    public @ResponseBody
    Result showHistory(@RequestParam int senderId,@RequestParam int recipientId){
        return Result.ok(newsBiz.showHistory(senderId,recipientId));
    }

    //查看某管理员历史聊天用户(只有普通用户)
    @GetMapping(value = {"/historyUsers"})
    public @ResponseBody
    Result historyUsers(@RequestParam int senderId){
        return Result.ok(newsBiz.historyUsers(senderId));
    }

//-------------我的收藏----------------------------------------------------------------------------------------

    //------------------------------------------------藏品----------------------------------------------------

    //显示收藏的藏品列表
    @GetMapping(value = {"/showCollectionListRelic"})
    public @ResponseBody
    Result showCollectionListRelic(@RequestParam int userId){
        return Result.ok(personalCollectionRelicBiz.showCollectionList(userId));
    }

    //显示具体藏品
    @GetMapping(value = {"/showSelectedCollectionRelic"})
    public @ResponseBody
    Result showSelectedCollectionRelic(@RequestParam int userId, @RequestParam int relicId){
        return Result.ok(personalCollectionRelicBiz.showSelectedCollection(userId,relicId));
    }

    //藏品计数
    @GetMapping(value = {"/sumCollectionRelic"})
    public @ResponseBody
    Result sumCollectionRelic(@RequestParam int userId){
        return Result.ok(personalCollectionRelicBiz.sumCollection(userId));
    }
    //收藏藏品
    @PostMapping(value = {"/collectRelic"})
    public @ResponseBody
    Result collectRelic(@RequestBody Map<String,Integer> map){
        PersonalCollectionRelic PCR = new PersonalCollectionRelic();
        PCR.setRelicId(map.get("relicId"));
        PCR.setUserId(map.get("userId"));
        return Result.ok(personalCollectionRelicBiz.collect(PCR));
    }
    //取消收藏
    @PostMapping(value = {"/deleteCollectionRelic"})
    public @ResponseBody
    Result deleteCollectionRelic(@RequestParam int userId,@RequestParam int relicId){

        return Result.ok(personalCollectionRelicBiz.deleteCollection(userId,relicId));
    }

    //搜索收藏藏品
    @GetMapping(value = {"/queryCollectionsRelic"})
    public @ResponseBody
    Result queryCollectionsRelic(@RequestParam int userId,@RequestParam String keyword,@RequestParam  String startTime,
                                 @RequestParam String endTime,@RequestParam  String type,@RequestParam  String time){
        //前端是设置一个下拉框选项为null代表所有吗？


        if (startTime.equals(null) || endTime.equals(null)||startTime.isEmpty()|| endTime.isEmpty()){

            startTime=(new Timestamp(0000-0-0)).toString();
            endTime = (new Timestamp(System.currentTimeMillis())).toString();

            return Result.ok(personalCollectionRelicBiz.queryCollections(userId, keyword, startTime, endTime, type, time));
        }
        return Result.ok(personalCollectionRelicBiz.queryCollections(userId, keyword, Timestamp.valueOf(startTime+" 00:00:00").toString(),
                Timestamp.valueOf(endTime+" 00:00:00").toString(),type, time));
    }

    //确认某人是否收藏过某文章，1为收藏过，0为未收藏
    @GetMapping(value = {"/whetherCollectedRelic"})
    public @ResponseBody
    Result whetherCollectedRelic(@RequestParam int relicId, @RequestParam int userId){
        return Result.ok(personalCollectionRelicBiz.whetherCollected(relicId,userId));
    }

    //------------------------------------------------文章----------------------------------------------------

    //显示收藏的文章列表
    @GetMapping(value = {"/showCollectionListEssay"})
    public @ResponseBody
    Result showCollectionListEssay(@RequestParam int userId){
        return Result.ok(personalCollectionEssayBiz.showCollectionList(userId));
    }

    //显示具体文章
    @GetMapping(value = {"/showSelectedCollectionEssay"})
    public @ResponseBody
    Result showSelectedCollectionEssay(@RequestParam int userId, @RequestParam int essayId){
        return Result.ok(personalCollectionEssayBiz.showSelectedCollection(userId,essayId));
    }

    //收藏文章计数
    @GetMapping(value = {"/sumCollectionEssay"})
    public @ResponseBody
    Result sumCollectionEssay(@RequestParam int userId){
        return Result.ok(personalCollectionEssayBiz.sumCollection(userId));
    }
    //收藏文章
    @PostMapping(value = {"/collectEssay"})
    public @ResponseBody
    Result collectEssay(@RequestBody Map<String,Integer> map){
        PersonalCollectionEssay PCE = new PersonalCollectionEssay();
        PCE.setEssayId(map.get("essayId"));
        PCE.setUserId(map.get("userId"));
        return Result.ok(personalCollectionEssayBiz.collect(PCE));
    }
    //取消收藏
    @PostMapping(value = {"/deleteCollectionEssay"})
    public @ResponseBody
    Result deleteCollectionEssay(@RequestBody Map<String,Integer> map){
        return Result.ok(personalCollectionEssayBiz.deleteCollection(map.get("userId"),map.get("essayId")));
    }

    //搜索收藏藏品
    @GetMapping(value = {"/queryCollectionsEssay"})
    public @ResponseBody
    Result queryCollectionsEssay(@RequestParam int userId,@RequestParam String keyword,@RequestParam  String startTime,
                                 @RequestParam String endTime){
        //前端是设置一个下拉框选项为null代表所有吗？


        if (startTime.equals(null) || endTime.equals(null)||startTime.isEmpty()|| endTime.isEmpty()){

            startTime=(new Timestamp(0000-0-0)).toString();
            endTime = (new Timestamp(System.currentTimeMillis())).toString();

            return Result.ok(personalCollectionEssayBiz.queryCollections(userId, keyword, startTime, endTime));
        }
        return Result.ok(personalCollectionEssayBiz.queryCollections(userId, keyword, Timestamp.valueOf(startTime+" 00:00:00").toString(),
                Timestamp.valueOf(endTime+" 00:00:00").toString()));
    }
    //确认某人是否收藏过某文章，1为收藏过，0为未收藏
    @GetMapping(value = {"/whetherCollectedEssay"})
    public @ResponseBody
    Result whetherCollectedEssay(@RequestParam int essayId, @RequestParam int userId){
        return Result.ok(personalCollectionEssayBiz.whetherCollected(essayId,userId));
    }
    //------------------------------------------------diy----------------------------------------------------

    //显示收藏的DIY作品列表
    @GetMapping(value = {"/showCollectionListDiy"})
    public @ResponseBody
    Result showCollectionListDiy(@RequestParam int userId){
        return Result.ok(personalCollectionDiyBiz.showCollectionList(userId));
    }

    //显示具体DIY作品
    @GetMapping(value = {"/showSelectedCollectionDiy"})
    public @ResponseBody
    Result showSelectedCollectionDiy(@RequestParam int userId, @RequestParam int showId){
        return Result.ok(personalCollectionDiyBiz.showSelectedCollection(userId,showId));
    }

    //收藏DIY作品计数
    @GetMapping(value = {"/sumCollectionDiy"})
    public @ResponseBody
    Result sumCollectionDiy(@RequestParam int userId){
        return Result.ok(personalCollectionDiyBiz.sumCollection(userId));
    }
    //收藏DIY作品
    @PostMapping(value = {"/collectDiy"})
    public @ResponseBody
    Result collectDiy(@RequestBody Map<String,Integer> map){
        PersonalCollectionDiy PCD = new PersonalCollectionDiy();
        PCD.setShowId(map.get("showId"));
        PCD.setUserId(map.get("userId"));
        return Result.ok(personalCollectionDiyBiz.collect(PCD));
    }
    //取消收藏
    @PostMapping(value = {"/deleteCollectionDiy"})
    public @ResponseBody
    Result deleteCollectionDiy (@RequestBody Map<String,Integer> map){

        return Result.ok(personalCollectionDiyBiz.deleteCollection(map.get("userId"),map.get("showId")));
    }

    //搜索收藏藏品
    @GetMapping(value = {"/queryCollectionsDiy"})
    public @ResponseBody
    Result queryCollectionsDiy(@RequestParam int userId,@RequestParam String keyword,@RequestParam  String startTime,
                                 @RequestParam String endTime){
        //前端是设置一个下拉框选项为null代表所有吗？


        if (startTime.equals(null) || endTime.equals(null)||startTime.isEmpty()|| endTime.isEmpty()){

            startTime=(new Timestamp(0000-0-0)).toString();
            endTime = (new Timestamp(System.currentTimeMillis())).toString();

            return Result.ok(personalCollectionDiyBiz.queryCollections(userId, keyword, startTime, endTime));
        }
        return Result.ok(personalCollectionDiyBiz.queryCollections(userId, keyword, Timestamp.valueOf(startTime+" 00:00:00").toString(),
                Timestamp.valueOf(endTime+" 00:00:00").toString()));
    }

    //确认某人是否收藏过某作品，1为收藏过，0为未收藏
    @GetMapping(value = {"/whetherCollectedDiy"})
    public @ResponseBody
    Result whetherCollectedDiy(@RequestParam int showId, @RequestParam int userId){
        return Result.ok(personalCollectionDiyBiz.whetherCollected(showId,userId));
    }
//--------------------------------------------我上传的文章----------------------------------------------

    //显示我上传的文章
    @GetMapping(value = {"/displayMyArticles"})
    public @ResponseBody
    Result displayMyArticles(@RequestParam int userId){
        return Result.ok(essayBiz.queryEssayByUserId(userId));
    }
}
