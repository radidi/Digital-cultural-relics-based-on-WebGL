package com.dcr.controller;

import com.dcr.biz.IPersonalCenterBiz;
import com.dcr.biz.IPersonalSecurityBiz;
import com.dcr.entity.PersonalCenter;
import com.dcr.entity.PersonalSecurity;
import com.dcr.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@CrossOrigin
@Controller
@RequestMapping("/PersonalSettings")
public class PersonalSettingsController {
    @Autowired
    private IPersonalCenterBiz personalCenterBiz;
    @Autowired
    private IPersonalSecurityBiz personalSecurityBiz;

//---------------------------------------修改用户信息-------------------------------------------------------------
    //用户自己修改信息
    @PostMapping(value = {"/changeInformationByUser"})
    public @ResponseBody
    Result<?> changeInformationByUser(@RequestBody Map<String,String> map)
    {
        PersonalCenter pc=new PersonalCenter();
        int userId = Integer.parseInt(map.get("userId"));
        pc.setUserId(userId);//前端应设置不可更改
        pc.setUsername(map.get("username"));
//        pc.setRole(map.get("role"));//前端应设置不可更改，前端显示该数据吗？
//        pc.setGrade(Integer.parseInt(map.get("grade")));
        pc.setPhoto(map.get("photo"));
        pc.setDownload(map.get("download"));
        String oldPass = map.get("oldPass");
        String newPass = map.get("newPass");
        if ( !newPass.isEmpty() && !oldPass.isEmpty() ){
            String info=personalCenterBiz.editPasswordByOld(userId,oldPass,newPass);
            if(info.equals("原密码错误！")){
                return Result.error(info);
            }
        }

        return Result.ok(personalCenterBiz.changeInformation(pc));

    }
    //管理员修改用户信息
    @PostMapping(value = {"/changeInformationByAdmin"})
    public @ResponseBody
    Result<?> changeInformationByAdmin(@RequestBody Map<String,String> map)
    {
        //前端应该先获取用户信息，显示在页面中表单项上，不然的话要重新再写一个管理员更新的业务层函数，回来讨论
        //大问题,前端如何同时分别传入用户和管理员数据?
        PersonalCenter pc=new PersonalCenter();

        int userId = Integer.parseInt(map.get("userId"));
        pc.setUserId(userId);
        pc.setRole(map.get("role"));
//        pc.setGrade(Integer.parseInt(map.get("grade")));//grade属性删掉了


        return Result.ok(personalCenterBiz.changeInformation(pc));

    }

//--------------------------------------------修改密码（根据旧的密码修改）-----------------------------------------
    //----------根据旧密码修改新密码-------------

    @PostMapping(value = {"/editPasswordByOld"})
    public @ResponseBody
    Result<?> editPasswordByOld(int userId,String oldPass, String newPass)
    {
        String result=personalCenterBiz.editPasswordByOld(userId, oldPass, newPass);
        if (result.equals("原密码错误！")){
           return Result.error(result);
        }
        return Result.ok("修改成功");

    }
    //----------根据密保问题修改密码------------（其实在LoginRegister里也有
    // ①获得密保问题
    @GetMapping(value = {"/getQuestions"})
    public @ResponseBody Result getQuestions(@RequestParam String username){
        List<PersonalSecurity> list = personalSecurityBiz.getAllQuestions(username);
        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }
        return Result.ok(list);
    }

    //②验证密保问题并修改密码
    @PostMapping(value = {"/confirmQuestionAndUpdatePassword"})
    public @ResponseBody
    Result confirmQuestionAndUpdatePassword(@RequestBody Map<String, String> map){
        String username=map.get("username");
        List<PersonalSecurity> list = personalSecurityBiz.getAllQuestions(username);
        for(int i=0;i<list.size();i++){
            if ((list.get(i).getQuestion().equals(map.get("question"+(i+1))))
                    &&!list.get(i).getAnswer().equals(map.get("answer"+(i+1)))){
                return Result.error("答案错误");
            }
        }

        PersonalCenter personalCenter =personalCenterBiz.queryUserByUsername(username);

        return Result.ok(personalCenterBiz.updatePassword(personalCenter.getUserId(),map.get("newPass")));
    }

//--------------------------------------------设置密保问题------------------------------------------------------

    //根据用户名获得密保问题和验证密保问题在上面↑
    //根据用户ID获得密保问题
    @GetMapping(value = {"/getQuestionsByUserId"})
    public @ResponseBody Result queryByUserId(@RequestParam int userId){
        List<PersonalSecurity> list = personalSecurityBiz.queryByUserId(userId);
        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }
        return Result.ok(list);
    }

    //添加密保问题
    @PostMapping(value = {"/addQuestion"})
    public @ResponseBody Result addQuestion(@RequestParam int userId, @RequestParam String question, @RequestParam String answer){
        int questionId = 1;
        if(personalSecurityBiz.queryByUserId(userId).size() != 0)
            questionId = personalSecurityBiz.getMaxQuestionId(userId) + 1;
        personalSecurityBiz.addQuestion(userId,questionId,question,answer);
        return Result.ok(personalSecurityBiz.queryByUserId(userId));
    }

    //修改密保问题
    @PostMapping(value = {"/editQuestion"})
    public @ResponseBody  Result editQuestion(@RequestParam int userId, @RequestParam int questionId,
                               @RequestParam String question, @RequestParam String answer){

        return Result.ok(personalSecurityBiz.editQuestion(userId,questionId,question,answer));
    }

    //删除密保问题
    @PostMapping(value = {"/deleteQuestion"})
    public @ResponseBody  Result deleteQuestion(@RequestParam int userId, @RequestParam int questionId){

        return Result.ok(personalSecurityBiz.deleteQuestion(userId,questionId));
    }
}
