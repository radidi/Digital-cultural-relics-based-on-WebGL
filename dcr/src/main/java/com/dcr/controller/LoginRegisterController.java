package com.dcr.controller;

import com.dcr.biz.IPersonalCenterBiz;
import com.dcr.biz.IPersonalSecurityBiz;
import com.dcr.entity.PersonalCenter;
import com.dcr.entity.PersonalSecurity;
import com.dcr.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@CrossOrigin
@Controller
@RequestMapping("/LoginRegister")
public class LoginRegisterController {
    @Autowired
    private IPersonalCenterBiz personalCenterBiz;
    @Autowired
    private IPersonalSecurityBiz personalSecurityBiz;

//----------------------------------------------登录注册------------------------------------------------------------------

    //login
    @PostMapping(value = {"/login"})
    public @ResponseBody
    Result<?> login(@RequestBody Map<String, String> map)
    {
        System.out.println(map);
        String username = map.get("username");
        String password = map.get("password");
        PersonalCenter personalCenter=personalCenterBiz.login(username,password);

        if (personalCenter == null){
            return Result.error("账号不存在或密码不正确");
        }else {
            return Result.ok(personalCenter);
        }


    }

    //注册
    @PostMapping(value = {"/register"})
    public @ResponseBody
    Result<?> register(@RequestBody Map<String, String> map)
    {
        System.out.println(map);
        String username = map.get("username");
        String password = map.get("password");
        int msg = personalCenterBiz.register(username, password);
        if (msg == -1)
            return Result.error("账号已存在");
        else
            return Result.ok(personalCenterBiz.login(username, password));
    }


//-----------------------------------找回密码-----------------------------------------------------------------------------
    //①获得密保问题
    @GetMapping(value = {"/getQuestions"})
    public @ResponseBody Result getQuestions(@RequestParam String username){
        List<PersonalSecurity> list = personalSecurityBiz.getAllQuestions(username);
        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }
        return Result.ok(list);
    }

    //②验证密保问题并修改密码
    @PostMapping(value = {"/confirmQuestionAndUpdatePassword"})
    public @ResponseBody
    Result confirmQuestionAndUpdatePassword(@RequestBody Map<String, String> map){
        String username=map.get("username");
        List<PersonalSecurity> list = personalSecurityBiz.getAllQuestions(username);
        for(int i=0;i<list.size();i++){
            if ((list.get(i).getQuestion().equals(map.get("问题"+(i+1))))
                    &&!(list.get(i).getAnswer().equals(map.get("回答"+(i+1))))){
                return Result.error("问题"+(i+1)+"答案不正确");
            }
        }

        PersonalCenter personalCenter =personalCenterBiz.queryUserByUsername(username);

        return Result.ok(personalCenterBiz.updatePassword(personalCenter.getUserId(),map.get("newPass")));
    }

}
