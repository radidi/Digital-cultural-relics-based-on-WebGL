package com.dcr.controller;

import com.dcr.biz.IDiyCreationBiz;
import com.dcr.entity.DiyCreation;
import com.dcr.entity.DiyCreationSearch;
import com.dcr.entity.PersonalCollectionDiy;
import com.dcr.entity.Relic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/diycreation")
/**
 * diy作品展示栏，里面展示用户作品
 */
public class DiyCreationController {

    @Autowired
    IDiyCreationBiz iDiyCreationBiz;
    /**
     *从展品栏删除
     * 注意必须输入当前用户id，因为这个函数只能删除自己的
     */
    @PostMapping("/deletework")
    @ResponseBody
    public int deletework(@RequestBody DiyCreation diyCreation){
        return iDiyCreationBiz.deletework(diyCreation.getShowId());
    }

    /**
     *搜索显示所有用户作品
     */
    @PostMapping("/findwork")
    @ResponseBody
    public List<DiyCreationSearch>  findwork(@RequestBody DiyCreationSearch diyCreationSearch){
        return iDiyCreationBiz.findwork(diyCreationSearch);
    }

    /**
     *批量删除我的作品
     */
    @PostMapping("/deleteworks")
    @ResponseBody
    public int deleteworks(@RequestBody List<Integer> list){
        for(int i=0;i<list.size();i++){
            DiyCreation diyCreation=new DiyCreation();
            diyCreation.setShowId(list.get(i));
            diyCreation.setUserId(1);
            iDiyCreationBiz.deletework(diyCreation.getShowId());
        }
        return 1;
    }
    /**
     *收藏diy作品
     */
    @PostMapping("/addDiyWork")
    @ResponseBody
    public int addDiyWork(@RequestBody PersonalCollectionDiy personalCollectionDiy){
        return iDiyCreationBiz.addDiyWork(personalCollectionDiy);
    }

    /**
     * 特殊功能
     * 根据id找作品
     */
    @PostMapping("/findDiyWorkById")
    @ResponseBody
    public DiyCreation findDiyWorkById(@RequestParam int showId){
        return iDiyCreationBiz.findDiyWorkById(showId);
    }


    /**
     * 根据userId，进行模糊搜索
     * 就是在个人作品页面（已经显示了该用户的所有作品），进一步精确搜索，即模糊搜索某个用户的作品
     */
    @PostMapping("/fuzzySearchPersonalDiy")
    @ResponseBody
    public List<DiyCreationSearch> fuzzySearchPersonalDiy(@RequestBody DiyCreationSearch diyCreationSearch){
       return iDiyCreationBiz.findwork(diyCreationSearch);
    }
}
