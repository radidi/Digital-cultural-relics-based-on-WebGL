package com.dcr.controller;

import com.dcr.biz.IRecommendBiz;
import com.dcr.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/RecommendManage")//社区推荐管理
@CrossOrigin
public class RecommendManageController {
    @Autowired
    private IRecommendBiz recommendBiz;

    //管理员查看推荐（30）
    @RequestMapping(value = "/MqueryRecommend",method = RequestMethod.GET)
    public Result<?> MqueryRecommend(){
        return Result.ok(recommendBiz.MqueryRecommend());
    }
    //修改推荐是否显示
    //status:1展示，0不展示
    @RequestMapping(value = "/updateRelicRecommendStatus",method = RequestMethod.POST)
    public @ResponseBody
    int updateRelicRecommendStatus(@RequestBody Map<String,String> map){
        return recommendBiz.updateRelicRecommendStatus(Integer.parseInt(map.get("id")),
                map.get("type"),map.get("status"));
    }
    //管理员查看某类推荐（30）
    //下拉式，自选类别：藏品，文章，diy
    @RequestMapping(value = "/MqueryRelicRecommend",method = RequestMethod.GET)
    public Result<?> MqueryRelicRecommend(String type){
        return Result.ok(recommendBiz.MqueryRecommendByType(type));
    }
}
