package com.dcr.controller;

import com.dcr.biz.IRecommendBiz;
import com.dcr.biz.impl.HomeRecommendBiz;
import com.dcr.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/HomePage")
@CrossOrigin
public class UserHomePageController {
    @Autowired
    private HomeRecommendBiz biz;
    @Autowired
    private IRecommendBiz recommendBiz;

    //用户看所有的推荐
    @GetMapping(value = {"/show"})
    public Result show(){
        return Result.ok(biz.show());
    }

    //客户根据关键词搜索，有点问题
    @GetMapping(value = {"/queryByKeyword"})
    public @ResponseBody
    Result queryByKeyword(@RequestParam String keyword){
        return Result.ok(recommendBiz.queryByKeyword(keyword));
    }







}
