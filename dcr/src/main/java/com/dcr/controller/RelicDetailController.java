package com.dcr.controller;

import com.dcr.biz.*;
import com.dcr.entity.*;
import com.dcr.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/RelicDetail")
@CrossOrigin
public class RelicDetailController {
    @Autowired
    private IRelicBiz relicBiz;
    @Autowired
    private IPersonalCollectionRelicBiz personalCollectionRelicBiz;
    @Autowired
    private IRelicCommentBiz relicCommentBiz;
    @Autowired
    private IRelicCommentWholeBiz relicCommentWholeBiz;
    @Autowired
    private IEssayBiz essayBiz;
    @Autowired
    private INewsBiz newsBiz;


    //查藏品详情
    @RequestMapping(value = "/showRelic",method = RequestMethod.GET)
    public Result<?> showRelic(int relicId){
        Relic relic = relicBiz.showRelic(relicId);
        return Result.ok(relic);
    }
    //收藏藏品,参数relicId、userId
    @RequestMapping(value = "/collect",method = RequestMethod.POST)
    public @ResponseBody
    int collect(@RequestBody Map<String,Integer> map){
        PersonalCollectionRelic pClc = new PersonalCollectionRelic();
        pClc.setRelicId(map.get("relicId"));
        pClc.setUserId(map.get("userId"));
        return personalCollectionRelicBiz.collect(pClc);
    }
    //添加评论,参数relicId,userId,content,replyId
    @RequestMapping(value = "/addComment",method = RequestMethod.POST)
    public @ResponseBody
    int addComment(@RequestBody RelicComment relicComment){
        relicCommentBiz.addComment(relicComment);
        return newsBiz.insertComment(relicComment);
    }
    //查询文物全部评论
    @RequestMapping(value = "/queryCommentWhole",method = RequestMethod.GET)
    public Result<?> queryCommentWhole(int relicId){
        List<RelicCommentWhole> relicCommentWholes = relicCommentWholeBiz.queryCommentWhole(relicId);
        return Result.ok(relicCommentWholes);
    }
    //评论点赞
    @RequestMapping(value = "/giveALike",method = RequestMethod.POST)
    public @ResponseBody
    int giveALike(@RequestParam(required = true) int commentId){
        return relicCommentBiz.giveALike(commentId);
    }
    //查藏品相关文物
    @RequestMapping(value = "/showRelativeRelic",method = RequestMethod.GET)
    public Result showRelativeRelic(int relicId){
        List<Relic> relics = relicBiz.showRelativeRelic(relicId);
        return Result.ok(relics);
    }
    //查藏品相关文章
    @RequestMapping(value = "/showRelativeEassy",method = RequestMethod.GET)
    public Result showRelativeEassy(int relicId){
        List<Essay> essays = essayBiz.showRelativeEassy(relicId);
        return Result.ok(essays);
    }
    //删除评论
    @RequestMapping(value = "/deleteRelicComment", method = RequestMethod.POST)
    public @ResponseBody
    int deleteRelicComment(@RequestParam(required = true) int commentId)
    {
        return relicCommentBiz.deleteRelicComment(commentId);
    }
    //藏品浏览量+1，并更新recommend表热度
    @RequestMapping(value = "/updateRelicViewsNum",method = RequestMethod.POST)
    public @ResponseBody
    int updateRelicViewsNum(@RequestParam(required = true) int relicId){
        return relicBiz.updateRelicViewsNum(relicId);
    }

}
