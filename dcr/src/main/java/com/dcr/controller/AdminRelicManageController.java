package com.dcr.controller;


import com.dcr.biz.impl.RelicBiz;
import com.dcr.entity.Relic;
import com.dcr.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/AdminRelicManage")
@CrossOrigin
public class AdminRelicManageController {

    @Autowired
    private RelicBiz relicBiz;
    //必须有String relicName,int relicId,String relicTime,String type,String relicPicture,String model,String introduce

    @RequestMapping(value="/addRelic", method = RequestMethod.POST)
    public @ResponseBody
    int addRelic(@RequestBody Relic relic){
        return relicBiz.addRelic(relic);
    }

    @RequestMapping(value = "/updateRelic", method = RequestMethod.GET)
    public Result updateRelic(@RequestParam int relicId,
                              @RequestParam String relicName,
                              @RequestParam (required = false,defaultValue = "") String relicTime,
                              @RequestParam String Type,
                              @RequestParam (required = false,defaultValue = "") String relicPicture,
                              @RequestParam (required = false,defaultValue = "") String model,
                              @RequestParam (required = false,defaultValue = "") String introduce){
        Relic relic = new Relic();
        relic.setRelicId(relicId);
        relic.setRelicName(relicName);
        relic.setRelicTime(relicTime);
        relic.setRelicType(Type);
        relic.setRelicPicture(relicPicture);
        relic.setModel(model);
        relic.setIntroduce(introduce);
        return Result.ok(relicBiz.updateRelic(relic));
    }
}
