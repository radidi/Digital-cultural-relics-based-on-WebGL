package com.dcr.controller;

import com.dcr.biz.impl.RelicBiz;
import com.dcr.entity.Relic;
import com.dcr.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/RelicClassification")
@CrossOrigin
public class RelicClassificationController {

    @Autowired
    private RelicBiz relicBiz;

    @RequestMapping(value = "/queryByType",method = RequestMethod.GET)
    public Result queryByType(@RequestParam String TypeName){
        return Result.ok(relicBiz.queryByType(TypeName));
    }


}
