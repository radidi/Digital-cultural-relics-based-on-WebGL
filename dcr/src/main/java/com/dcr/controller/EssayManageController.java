package com.dcr.controller;

import com.dcr.biz.IEssayBiz;
import com.dcr.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/EssayManage")
@CrossOrigin
public class EssayManageController {
    @Autowired
    private IEssayBiz essayBiz;
    //删除文章
    @RequestMapping(value = "/deleteEassy" ,method = RequestMethod.POST)
    public @ResponseBody
    int deleteEassy(@RequestBody List<Integer> essayIds){
        for(int essayId:essayIds){
            essayBiz.deleteEassy(essayId);
        }
        return 1;
    }

    //搜索文章
    @RequestMapping(value = "/queryEssay",method = RequestMethod.GET)
    public Result<?> queryEssay(String title){
        return Result.ok(essayBiz.queryEssay(title));
    }
}
