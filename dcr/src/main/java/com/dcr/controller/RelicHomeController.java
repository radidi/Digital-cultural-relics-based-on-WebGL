package com.dcr.controller;

import com.dcr.biz.IRecommendBiz;
import com.dcr.biz.IRelicBiz;
import com.dcr.entity.Relic;
import com.dcr.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/RelicHome")
@CrossOrigin
public class RelicHomeController {
    @Autowired
    private IRecommendBiz recommendBiz;
    @Autowired
    private IRelicBiz relicBiz;

    //查看藏品推荐(前8)
    @RequestMapping(value = "/queryRelicRecommend",method = RequestMethod.GET)
    public Result queryRelicRecommend(){
        List<Relic> relics = recommendBiz.queryRelicRecommend();
        return Result.ok(relics);
    }
    //搜索藏品
    @RequestMapping(value = "/queryRelic",method = RequestMethod.GET)
    public Result queryRelic(@RequestParam(required = false,defaultValue = "") String relicName,
                             @RequestParam(required = false,defaultValue = "") String relicTime,
                             @RequestParam(required = false,defaultValue = "") String relicType){
        List<Relic> relics=relicBiz.queryRelic(relicName,relicTime,relicType);
        return Result.ok(relics);
    }
}
