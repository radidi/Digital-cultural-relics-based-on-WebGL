package com.dcr.controller;

import com.dcr.biz.impl.RecommendBiz;
import com.dcr.entity.Recommend;
import com.dcr.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/AdminRelicRecommend")
@CrossOrigin
public class AdminRelicRecommendController {

    @Autowired
    private RecommendBiz recommendBiz;

    @RequestMapping(value = "/updateRelicRecommendStatus",method = RequestMethod.GET)
    public Result queryByType(@RequestParam int id, @RequestParam String status){
        return Result.ok(recommendBiz.updateRelicRecommendStatus(id,"藏品",status));
    }

}
