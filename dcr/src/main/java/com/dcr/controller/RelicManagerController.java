package com.dcr.controller;

import com.dcr.biz.IRelicBiz;
import com.dcr.entity.Relic;
import com.dcr.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/RelicManager")
@CrossOrigin
public class RelicManagerController {
    @Autowired
    private IRelicBiz relicBiz;

    //搜索藏品
    @RequestMapping(value = "/queryRelic",method = RequestMethod.GET)
    public Result queryRelic(@RequestParam(required = false,defaultValue = "") String relicName,
                             @RequestParam(required = false,defaultValue = "") String relicTime,
                             @RequestParam(required = false,defaultValue = "") String relicType){
        List<Relic> relics=relicBiz.queryRelic(relicName,relicTime,relicType);
        return Result.ok(relics);
    }
    //修改藏品
    @RequestMapping(value = "/updateRelic",method = RequestMethod.POST)
    public @ResponseBody
    int updateRelic(@RequestBody Relic relic){
        return relicBiz.updateRelic(relic);
    }
    //删除藏品
    @RequestMapping(value = "/deleteRelic",method = RequestMethod.POST)
    public @ResponseBody
    int deleteRelic(@RequestParam(required = true) int relicId){
        return relicBiz.deleteRelic(relicId);
    }
}
