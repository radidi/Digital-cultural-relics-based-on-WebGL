package com.dcr.controller;

import com.dcr.biz.ICreativeWorkshopBiz;
import com.dcr.biz.IRelicBiz;
import com.dcr.entity.CreativeWorkshopSearch;
import com.dcr.entity.DiyCreation;
import com.dcr.entity.PersonalCollectionRelic;
import com.dcr.entity.Relic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/creativeWorkshop")
/**
 * diy创作界面，里面是提供可创作的模型
 */
public class CreativeWorkshopController {

    @Autowired
    ICreativeWorkshopBiz iCreativeWorkshopBiz;
    @Autowired
    IRelicBiz iRelicBiz;

    /**
     * 查询功能
     */
    @PostMapping("/findwork")
    @ResponseBody
    public List<Relic> findwork(@RequestBody CreativeWorkshopSearch creativeWorkshopSearch){
        return iCreativeWorkshopBiz.findwork(creativeWorkshopSearch);
    }

    /**
     *添加到展示栏
     */
    @PostMapping("/addshow")
    @ResponseBody
    public int addshow(@RequestBody DiyCreation diyCreation){
        return iCreativeWorkshopBiz.addshow(diyCreation);
    }

    /**
     *收藏藏品
     */
    @PostMapping("/addcollection")
    @ResponseBody
    public int addcollection(@RequestBody PersonalCollectionRelic personalCollectionRelic){
        return iCreativeWorkshopBiz.addcollection(personalCollectionRelic);
    }

    /**
     *查找藏品
     */
    @PostMapping("/findRelicById")
    @ResponseBody
    public Relic findRelicById(@RequestParam int relicId){
        return iRelicBiz.showRelic(relicId);
    }

}
