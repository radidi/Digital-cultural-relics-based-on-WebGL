package com.dcr.controller;

import com.dcr.biz.IRecommendBiz;
import com.dcr.entity.Recommend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Recommend")
@CrossOrigin
public class RecommendController {
    @Autowired
    IRecommendBiz iRecommendBiz;



    //查前15推荐
    @GetMapping("/queryRecommend")
    public @ResponseBody
    List<Recommend> queryRecommend(){
        return iRecommendBiz.queryRecommend();
    }



}
