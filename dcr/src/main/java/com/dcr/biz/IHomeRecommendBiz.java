package com.dcr.biz;

import com.dcr.entity.HomeRecommend;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface IHomeRecommendBiz {

    List<HomeRecommend> show();
    List<HomeRecommend> queryAll();
    List<HomeRecommend> queryByMaster(@Param("type") String type, @Param("key") String key);
    int add(HomeRecommend homeRecommend);
    int editById(int id, int status, String introduction,  String image);
    int deleteById(int id);
    int deleteBatch(List<Integer> ids);
    int openRecommend(int id);
    int closeRecommend(int id);

}
