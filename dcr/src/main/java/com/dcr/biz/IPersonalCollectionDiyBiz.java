package com.dcr.biz;

import com.dcr.entity.PersonalCollectionDiy;

import java.util.List;

public interface IPersonalCollectionDiyBiz {
    /**
     * 显示收藏的DIY作品列表
     */
    List<PersonalCollectionDiy> showCollectionList(int userId);

    /**
     * 显示具体DIY作品
     */
    PersonalCollectionDiy showSelectedCollection(int userId, int showId);
    /**
     * DIY作品计数
     */
    int sumCollection(int userId);
    /**
     * 收藏DIY作品
     */
    int collect(PersonalCollectionDiy PCD);

    /**
     * 取消收藏
     */
    int deleteCollection(int userId,int showId);

    /**
     *
     * 查询个人收藏的diy作品
     */
    List<PersonalCollectionDiy> queryCollections(int userId, String keyword, String startTime, String endTime);


    //删除作品后，级联删除收藏
    int deleteCollectionBatch(int showId);


    //确认某人是否收藏过某作品，1为收藏过，0为未收藏
    int whetherCollected(int showId,int userId);

    //根据relicId删除作品的收藏
    int deleteBatchByRelicId(int relicId);
}
