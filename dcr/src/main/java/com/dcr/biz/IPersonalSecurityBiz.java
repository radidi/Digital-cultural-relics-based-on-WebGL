package com.dcr.biz;

import com.dcr.entity.PersonalSecurity;

import java.util.List;
import java.util.Map;

public interface IPersonalSecurityBiz {
//    /**弃用了
//     * 找回、修改密码
//     * 设置每次随机回答一个问题
//     */
//    int findOrUpdatePassword(String username);

    /**
     * 找出所有密保问题
     * @return
     */
    List<PersonalSecurity> getAllQuestions(String username);
//    List<Map<String,String>> getAllQuestions(String username);
    /**
     * 添加密保问题
     */
    int addQuestion(int userId, int questionId, String question, String answer);
    /**
     * 修改密保问题
     */
    int editQuestion(int userId, int questionId, String question, String answer);
    /**
     * 删除密保问题
     */
    int deleteQuestion(int userId,int questionId);
    /**
     * 获取questionId最大值
     */
    int getMaxQuestionId(int userId);

    /**
     * 根据userId查询密保问题
     */
    List<PersonalSecurity> queryByUserId(int userId);

}
