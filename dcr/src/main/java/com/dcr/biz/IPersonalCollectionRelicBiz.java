package com.dcr.biz;

import com.dcr.entity.PersonalCollectionRelic;

import java.util.List;

public interface IPersonalCollectionRelicBiz {
    /**
     * 显示收藏的藏品列表
     */
    List<PersonalCollectionRelic> showCollectionList(int userId);

    /**
     * 显示具体藏品
     */
    PersonalCollectionRelic showSelectedCollection(int userId, int relicId);
    /**
     * 藏品计数
     */
    int sumCollection(int userId);
    /**
     * 收藏藏品
     */
    int collect(PersonalCollectionRelic pClc);

    /**
     * 取消收藏
     */
    int deleteCollection(int userId,int relicId);

    /**
     * 搜索收藏藏品
     * time:时代
     * start/endTime：
     */
    List<PersonalCollectionRelic> queryCollections(int userId, String keyword, String startTime, String endTime, String type, String time);

    //删除藏品后，级联删除收藏
    int deleteCollectionBatch(int relicId);

    //确认某人是否收藏过某藏品，1为收藏过，0为未收藏
    int whetherCollected(int relicId,int userId);


}
