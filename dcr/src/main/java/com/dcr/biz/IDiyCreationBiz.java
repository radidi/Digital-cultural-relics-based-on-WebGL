package com.dcr.biz;

import com.dcr.entity.DiyCreation;
import com.dcr.entity.DiyCreationSearch;
import com.dcr.entity.PersonalCollectionDiy;
import com.dcr.entity.PersonalCollectionRelic;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface IDiyCreationBiz {

    /**
     * 显示个人中心创作的所有作品
     */
    List<DiyCreation> showPersonalCreation(int userId);

    /**
     *显示具体作品
     */
    DiyCreation showSelectedCreation(int showId);

    /**
     * 作品计数
     */
    int sumCreation(int userId);
    /**
     * 编辑作品信息
     */
    int updateCreation(DiyCreation diyCreation);
    /**
     * lwj
     * 从展品栏删除展品
     */
    int deletework(int showId);


    /**
     * 搜索，显示用户作品
     */
    List<DiyCreationSearch> findwork(DiyCreationSearch diyCreationSearch);

    int addDiyWork(PersonalCollectionDiy personalCollectionDiy);

    DiyCreation findDiyWorkById(int showId);

}
