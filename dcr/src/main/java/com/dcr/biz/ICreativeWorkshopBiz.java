package com.dcr.biz;

import com.dcr.entity.CreativeWorkshopSearch;
import com.dcr.entity.DiyCreation;
import com.dcr.entity.PersonalCollectionRelic;
import com.dcr.entity.Relic;

import java.util.List;

public interface ICreativeWorkshopBiz {


     List<Relic> findwork(CreativeWorkshopSearch creativeWorkshopSearch);
    int addshow(DiyCreation diyCreation);
    int addcollection( PersonalCollectionRelic personalCollectionRelic);
}
