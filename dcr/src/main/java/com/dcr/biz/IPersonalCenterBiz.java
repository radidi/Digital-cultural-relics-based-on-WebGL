package com.dcr.biz;

import com.dcr.entity.DiyCreation;
import com.dcr.entity.PersonalCenter;

import java.util.List;

public interface IPersonalCenterBiz {

    /**
     *     登录
     */
    PersonalCenter login(String username,String password);
    /**
     * 注册
     */
    int register(String username,String password);
    /**
     * 展示用户信息
     */
    PersonalCenter showData(int userId);

    //根据用户名查询用户信息
    PersonalCenter queryUserByUsername(String username);

    /**
     * 验证密保问题后修改密码
     */
    int updatePassword(int userId,String newPass);

    /**
     * 根据旧密码修改密码
     */
    String editPasswordByOld(int userId,String oldPass,String newPass);


    /**
     * 修改个人信息
     * @param pc
     * @return
     */
    int changeInformation(PersonalCenter pc);


    //获得所有用户ID
    List<Integer> getAllUsersId();

    //获得所有普通用户信息
    List<PersonalCenter> getAllOrdinaryUsers();


}
