package com.dcr.biz;

import com.dcr.entity.Essay;

import java.util.List;

public interface IEssayBiz {
    //查藏品相关文章
    List<Essay> showRelativeEassy(int relicId);
    //搜索指定文章
    Essay queryEssayById(int essayId);
    //上传文章
    int insertEssay(Essay essay);
    //编辑文章
    int updateEssay(Essay essay);
    //删除文章
    int deleteEassy(int essayId);

    //文章浏览量+1，并更新recommend表热度
    int updateEssayViewsNum(int essayId);
    //按用户id查找文章
    List<Essay> queryEssayByUserId(int userId);
    //用标题搜索文章
    List<Essay> queryEssay(String title);
}
