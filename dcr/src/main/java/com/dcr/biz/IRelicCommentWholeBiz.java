package com.dcr.biz;

import com.dcr.entity.RelicCommentWhole;

import java.util.List;

public interface IRelicCommentWholeBiz {
    //查询文物全部评论
    List<RelicCommentWhole> queryCommentWhole(int relicId);
}
