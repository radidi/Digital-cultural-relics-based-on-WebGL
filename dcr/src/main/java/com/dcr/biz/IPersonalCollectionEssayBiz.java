package com.dcr.biz;

import com.dcr.entity.PersonalCollectionEssay;
import com.dcr.entity.PersonalCollectionRelic;

import java.util.List;

public interface IPersonalCollectionEssayBiz {
    /**
     * 显示收藏的文章列表
     */
    List<PersonalCollectionEssay> showCollectionList(int userId);

    /**
     * 显示具体文章
     */
    PersonalCollectionEssay showSelectedCollection(int userId, int essayId);
    /**
     * 文章计数
     */
    int sumCollection(int userId);
    /**
     * 收藏文章
     */
    int collect(PersonalCollectionEssay PCE);

    /**
     * 取消收藏
     */
    int deleteCollection(int userId,int essayId);

    /**
     *
     * 搜索文章
     */
    List<PersonalCollectionEssay> queryCollections(int userId, String keyword, String startTime, String endTime);

    //删除文章后，级联删除收藏
    int deleteCollectionBatch(int essayId);


    //确认某人是否收藏过某文章，1为收藏过，0为未收藏
    int whetherCollected(int essayId,int userId);

}
