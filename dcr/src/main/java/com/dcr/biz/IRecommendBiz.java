package com.dcr.biz;

import com.dcr.entity.HomeRecommend;
import com.dcr.entity.Recommend;
import com.dcr.entity.Relic;

import java.util.List;

public interface IRecommendBiz {
    //修改推荐是否显示
    int updateRelicRecommendStatus(int id,String type,String status);
    //查前15推荐
    List<Recommend> queryRecommend();
    //查看藏品推荐(前8)
    List<Relic> queryRelicRecommend();
    //管理员
    //管理员查看藏品推荐（30）
    List<Relic> MqueryRelicRecommend();
    //管理员查看推荐（30）
    List<Recommend> MqueryRecommend();
    //管理员查看某类推荐（30）
    List<Recommend> MqueryRecommendByType(String type);

    //根据关键词搜索
    List<Recommend> queryByKeyword(String keyword);

}
