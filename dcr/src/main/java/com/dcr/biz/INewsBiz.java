package com.dcr.biz;

import com.dcr.entity.News;
import com.dcr.entity.PersonalCenter;
import com.dcr.entity.RelicComment;

import java.util.List;

public interface INewsBiz {
    /**
     * 显示发出消息
     */
    List<News> showSentNews(int senderId);
    /**
     * 显示最近聊天用户
     */

    /**
     * 显示全部接收消息
     */
    List<News> showReceivedNews(int recipientId);

    /**
     * 显示接收到的管理员消息
     */
    List<News> showReceivedAdminNews(int recipientId);

    //显示接收到的管理员消息数
    int sumReceivedAdminNews(int recipientId);

    /**
     * 显示接收到的用户消息
     */
    List<News> showReceivedUserNews(int recipientId);

    //显示接收到的用户消息数
    int sumReceivedUserNews(int recipientId);
    /**
     * 显示具体消息
     */
    News showNewsDetail(News news);

    /**
     * 更新消息状态
     */
    int updateNewsStatus(int newsId);

    /**
     * 根据NewsId查询消息
     */
    News queryByNewsId(int newsId);

    /**
     * 已发送消息计数
     */
    int sumSentNews(int senderId);

    /**
     * 收到的消息计数
     */
    int sumReceivedNews(int recipientId);

    /**
     * 未读消息计数
     */
    int sumUnreadNews(int recipientId);

    /**
     * 未读管理员消息计数
     */
    int sumUnreadAdminNews(int recipientId);

    /**
     * 未读用户消息计数
     */
    int sumUnreadUserNews(int recipientId);
    /**
     * 显示未读的管理员消息
     */
    List<News> showUnreadAdminNews(int recipientId);
    /**
     * 显示未读的用户消息
     */
    List<News> showUnreadUserNews(int recipientId);


    /**
     * 已读消息计数
     */
    int sumReadNews(int recipientId);

    /**
     * 已读管理员消息计数
     */
    int sumReadAdminNews(int recipientId);

    /**
     * 已读用户消息计数
     */
    int sumReadUserNews(int recipientId);
    /**
     * 显示已读的管理员消息
     */
    List<News> showReadAdminNews(int recipientId);
    /**
     * 显示已读的用户消息
     */
    List<News> showReadUserNews(int recipientId);

    /**
     * 删除选定消息
     */
    int deleteSelectedNews(int newsId);

    /**
     * 批量删除消息
     */
    int deleteBatchNews(List<Integer> ids);


    /**
     * 获取最大消息ID
     */
    //int getMaxNewsId();

    /**
     * 管理员发送消息
     */
    int sendNews(News news);

    /**
     * 管理员批量发送消息
     */
    int masterSendNewsBatch(List<News> news);

    /**
     * 管理员给所有人发送消息
     */
    int masterSendNewsToAll(List<News> newsList);


    /**
     * 将收到的评论/回复添加到消息表中
     */
    int insertComment(RelicComment relicComment);

    /**
     * 显示历史消息
     */
    List<News> showHistory(int senderId,int recipientId);

    /**
     * 查看某管理员历史聊天用户(只有普通用户)
     */
    List<PersonalCenter> historyUsers(int senderId);
}
