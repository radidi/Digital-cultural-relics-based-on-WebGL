package com.dcr.biz;

import com.dcr.entity.RelicComment;

import java.sql.Timestamp;
import java.util.List;

public interface IRelicCommentBiz {
    //添加评论
    int addComment(RelicComment relicComment);
    //评论点赞
    int giveALike(int commentId);
    //查询回复列表,废弃
    //List<RelicComment> queryComment(int replyId);
    //删除评论
    int deleteRelicComment(int commentId);
    //根据replyid查找userid
    int queryUserIdByReplyId(int replyId);

}
