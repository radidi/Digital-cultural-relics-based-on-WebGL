package com.dcr.biz;

import com.dcr.entity.Relic;

import java.util.List;

public interface IRelicBiz {
    //搜索藏品
    List<Relic> queryRelic(String relicName, String relicTime, String relicType);
    //查某一类的所有藏品
    List<Relic> queryByType(String relicType);
    //查藏品详情
    Relic showRelic(int relicId);
    //添加藏品
    int addRelic(Relic relic);
    //修改藏品
    int updateRelic(Relic relic);
    //查藏品相关文物
    List<Relic> showRelativeRelic(int relicId);

    //藏品浏览量+1，并更新recommend表热度
    int updateRelicViewsNum(int relicId);

    //删除藏品
    int deleteRelic(int relicId);
}
