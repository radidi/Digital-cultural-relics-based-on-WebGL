package com.dcr.biz.impl;

import com.dcr.biz.IHomeRecommendBiz;
import com.dcr.entity.HomeRecommend;
import com.dcr.mapper.HomeRecommendMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HomeRecommendBiz implements IHomeRecommendBiz {

    @Autowired
    private HomeRecommendMapper mapper;


    @Override
    public List<HomeRecommend> show() {
        return mapper.show();
    }

    @Override
    public List<HomeRecommend> queryAll() {
        return mapper.queryAll();
    }

    @Override
    public List<HomeRecommend> queryByMaster(String type, String key) {
        return mapper.queryByMaster(type,key);
    }

    @Override
    public int add(HomeRecommend homeRecommend) {
        mapper.add(homeRecommend);
        return 1;
    }

    @Override
    public int editById(int id, int status, String introduction,  String  image) {
        HomeRecommend homeRecommend = new HomeRecommend();
        homeRecommend.setId(id);
        homeRecommend.setStatus(status);
        homeRecommend.setIntroduction(introduction);
        homeRecommend.setImage(image);
        mapper.editById(homeRecommend);
        return 1;
    }

    @Override
    public int deleteById(int id) {
        return 0;
    }

    @Override
    public int deleteBatch(List<Integer> ids) {
        return 0;
    }

    @Override
    public int openRecommend(int id) {
        HomeRecommend homeRecommend = new HomeRecommend();
        homeRecommend.setId(id);
        homeRecommend.setStatus(1);
        return mapper.openRecommend(homeRecommend);
    }

    @Override
    public int closeRecommend(int id) {
        HomeRecommend homeRecommend = new HomeRecommend();
        homeRecommend.setId(id);
        homeRecommend.setStatus(0);
        return mapper.closeRecommend(homeRecommend);
    }


}
