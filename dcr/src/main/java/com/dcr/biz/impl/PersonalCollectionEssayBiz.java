package com.dcr.biz.impl;

import com.dcr.biz.IPersonalCollectionEssayBiz;
import com.dcr.entity.PersonalCollectionEssay;
import com.dcr.mapper.PersonalCollectionEssayMapper;
import com.dcr.mapper.RecommendMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class PersonalCollectionEssayBiz implements IPersonalCollectionEssayBiz {
    @Autowired
    private PersonalCollectionEssayMapper personalCollectionEssayMapper;
    @Autowired
    private RecommendMapper recommendMapper;

    @Override
    public List<PersonalCollectionEssay> showCollectionList(int userId) {
        return personalCollectionEssayMapper.showCollectionList(userId);
    }

    @Override
    public PersonalCollectionEssay showSelectedCollection(int userId, int essayId) {
        return personalCollectionEssayMapper.showSelectedCollection(userId,essayId);
    }

    @Override
    public int sumCollection(int userId) {

        return  personalCollectionEssayMapper.sumCollection(userId);
    }

    //暂时先放着，收藏数+1的问题没解决
    @Override
    public int collect(PersonalCollectionEssay PCE) {
        Timestamp collectionTime=new Timestamp(System.currentTimeMillis());
        PCE.setCollectionTime(collectionTime);
        personalCollectionEssayMapper.collect(PCE);

        recommendMapper.updateRelicCollectionNum(PCE.getEssayId(),"文章");
        return 0;
    }

    //暂时先放着，收藏数-1的问题没解决
    @Override
    public int deleteCollection(int userId, int essayId) {
        personalCollectionEssayMapper.deleteCollection(userId,essayId);
        recommendMapper.reduceCollectionNum(essayId,"文章");
        return 0;
    }

    @Override
    public List<PersonalCollectionEssay> queryCollections(int userId, String keyword, String startTime, String endTime) {
        return personalCollectionEssayMapper.queryCollections(userId,keyword,startTime,endTime);
    }

    @Override
    public int deleteCollectionBatch(int essayId) {
        return personalCollectionEssayMapper.deleteCollectionBatch(essayId);

    }

    @Override
    public int whetherCollected(int essayId, int userId) {
        if(personalCollectionEssayMapper.whetherCollected(essayId,userId)==null){
            return 0;
        }else {
            return 1;
        }
    }


}
