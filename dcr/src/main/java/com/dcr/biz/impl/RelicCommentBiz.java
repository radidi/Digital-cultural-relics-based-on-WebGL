package com.dcr.biz.impl;

import com.dcr.biz.IRelicCommentBiz;
import com.dcr.entity.RelicComment;
import com.dcr.mapper.RelicCommentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;

@Service
public class RelicCommentBiz implements IRelicCommentBiz {
    @Autowired
    private RelicCommentMapper relicCommentMapper;
    @Override
    public int addComment(RelicComment relicComment) {
        //id
        relicComment.setCommentId(1);
        if(relicCommentMapper.countRelicCommentNum()!=0){
            relicComment.setCommentId(relicCommentMapper.maxRelicCommentId()+1);
        }
        relicComment.setTime(new Timestamp(System.currentTimeMillis()));
        return relicCommentMapper.addComment(relicComment);
    }
    @Override
    public int giveALike(int commentId) {
        return relicCommentMapper.giveALike(commentId);
    }

    //@Override
    //public List<RelicComment> queryComment(int replyId) {
    //    return relicCommentMapper.queryComment(replyId);
    //}

    @Override
    public int deleteRelicComment(int commentId) {
        relicCommentMapper.deleteRelicCommentReply(commentId);
        return relicCommentMapper.deleteRelicComment(commentId);
    }

    @Override
    public int queryUserIdByReplyId(int replyId) {
        return relicCommentMapper.queryUserIdByReplyId(replyId);
    }

}
