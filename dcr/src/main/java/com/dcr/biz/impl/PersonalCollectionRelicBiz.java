package com.dcr.biz.impl;

import com.dcr.biz.IPersonalCollectionRelicBiz;
import com.dcr.entity.PersonalCollectionRelic;
import com.dcr.mapper.PersonalCollectionDiyMapper;
import com.dcr.mapper.PersonalCollectionRelicMapper;
import com.dcr.mapper.RecommendMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class PersonalCollectionRelicBiz implements IPersonalCollectionRelicBiz {
    @Autowired
    private PersonalCollectionRelicMapper personalCollectionRelicMapper;
    @Autowired
    private PersonalCollectionDiyMapper personalCollectionDiyMapper;
    @Autowired
    private RecommendMapper recommendMapper;
    @Override
    public List<PersonalCollectionRelic> showCollectionList(int userId) {
        return personalCollectionRelicMapper.showCollectionList(userId);
    }

    @Override
    public PersonalCollectionRelic showSelectedCollection(int userId, int relicId) {
        return personalCollectionRelicMapper.showSelectedCollection(userId,relicId);
    }

    @Override
    public int sumCollection(int userId) {

        return  personalCollectionRelicMapper.sumCollection(userId);
    }

    //暂时先放着，收藏数+1的问题没解决
    @Override
    public int collect(PersonalCollectionRelic PCR) {
        Timestamp collectionTime=new Timestamp(System.currentTimeMillis());
        PCR.setCollectionTime(collectionTime);
        personalCollectionRelicMapper.collect(PCR);

        recommendMapper.updateRelicCollectionNum(PCR.getRelicId(),"藏品");
        return 0;
    }

    //暂时先放着，收藏数-1的问题没解决
    @Override
    public int deleteCollection(int userId, int relicId) {
        personalCollectionRelicMapper.deleteCollection(userId,relicId);
        recommendMapper.reduceCollectionNum(relicId,"藏品");
        return 0;
    }

    @Override
    public List<PersonalCollectionRelic> queryCollections(int userId, String keyword, String startTime, String endTime, String relicType, String time) {

        return personalCollectionRelicMapper.queryCollections(userId, keyword, startTime, endTime, relicType, time);
    }

    @Override
    public int deleteCollectionBatch(int relicId) {
         personalCollectionRelicMapper.deleteCollectionBatch(relicId);
         return personalCollectionDiyMapper.deleteBatchByRelicId(relicId);
    }

    @Override
    public int whetherCollected(int relicId, int userId) {
        if(personalCollectionRelicMapper.whetherCollected(relicId,userId)==null){
            return 0;
        }else {
            return 1;
        }
    }
}
