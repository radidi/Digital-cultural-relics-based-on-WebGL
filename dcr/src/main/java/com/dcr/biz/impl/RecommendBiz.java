package com.dcr.biz.impl;

import com.dcr.biz.IRecommendBiz;
import com.dcr.entity.Recommend;
import com.dcr.entity.Relic;
import com.dcr.mapper.RecommendMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecommendBiz implements IRecommendBiz {
    @Autowired
    private RecommendMapper recommendMapper;
    @Override
    public int updateRelicRecommendStatus(int id,String type,String status) {
        return recommendMapper.updateRelicRecommendStatus(id,type,status);
    }

    @Override
    public List<Recommend> queryRecommend() {
        return recommendMapper.queryRecommend();
    }

    @Override
    public List<Relic> queryRelicRecommend() {
        return recommendMapper.queryRelicRecommend();
    }

    @Override
    public List<Relic> MqueryRelicRecommend() {
        return recommendMapper.MqueryRelicRecommend();
    }

    @Override
    public List<Recommend> MqueryRecommend() {
        return recommendMapper.MqueryRecommend();
    }

    @Override
    public List<Recommend> MqueryRecommendByType(String type) {
        return recommendMapper.MqueryRecommendByType(type);
    }

    @Override
    public List<Recommend> queryByKeyword(String keyword) {
        return recommendMapper.queryByKeyword(keyword);
    }
}
