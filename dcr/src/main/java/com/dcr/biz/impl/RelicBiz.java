package com.dcr.biz.impl;

import com.dcr.biz.IDiyCreationBiz;
import com.dcr.biz.IRelicBiz;
import com.dcr.entity.Relic;
import com.dcr.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RelicBiz implements IRelicBiz {
    @Autowired
    private RelicMapper relicMapper;
    @Autowired
    private RecommendMapper recommendMapper;
    @Autowired
    private EssayMapper essayMapper;
    @Autowired
    private RelicCommentMapper relicCommentMapper;
    @Autowired
    private DiyCreationMapper diyCreationMapper;
    @Autowired
    private IDiyCreationBiz iDiyCreationBiz;
    @Autowired
    private PersonalCollectionRelicMapper personalCollectionRelicMapper;

    @Override
    public List<Relic> queryRelic(String relicName, String relicTime, String relicType) {
        return relicMapper.queryRelic(relicName,relicTime,relicType);
    }

    @Override
    public List<Relic> queryByType(String relicType) {
        return relicMapper.queryRelic(null,null,relicType);
    }

    @Override
    public Relic showRelic(int relicId) {
        return relicMapper.showRelic(relicId);
    }

    @Override
    public int addRelic(Relic relic) {
        //获取id
        relic.setRelicId(1);
        if(relicMapper.countRelicNum()!=0){
            relic.setRelicId(relicMapper.maxRelicId()+1);
        }
        return relicMapper.addRelic(relic);
    }

    @Override
    public int updateRelic(Relic relic) {
        return relicMapper.updateRelic(relic);
    }

    @Override
    public List<Relic> showRelativeRelic(int relicId) {
        return relicMapper.showRelativeRelic(relicId);
    }

    @Override
    public int updateRelicViewsNum(int relicId) {
        return recommendMapper.updateViewsNum(relicId,"藏品");
    }

    @Override
    public int deleteRelic(int relicId) {
        //关联文章relicid修改为0
        essayMapper.updateEssayRelicId(relicId);
        //删除diy
        int[] showIds = diyCreationMapper.queryShowIdByRlicId(relicId);
        for(int showId:showIds){
            iDiyCreationBiz.deletework(showId);
        }
        //删除评论
        relicCommentMapper.deleteRelicCommentByRelicId(relicId);
        //删除推荐
        recommendMapper.deleteRecommend(relicId,"藏品");
        //删除藏品后，级联删除收藏
        personalCollectionRelicMapper.deleteCollectionBatch(relicId);

        return relicMapper.deleteRelic(relicId);
    }
}
