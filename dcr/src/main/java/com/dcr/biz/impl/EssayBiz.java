package com.dcr.biz.impl;

import com.dcr.biz.IEssayBiz;
import com.dcr.entity.Essay;
import com.dcr.mapper.EssayMapper;
import com.dcr.mapper.PersonalCollectionEssayMapper;
import com.dcr.mapper.RecommendMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EssayBiz implements IEssayBiz {
    @Autowired
    private EssayMapper essayMapper;
    @Autowired
    private RecommendMapper recommendMapper;
    @Autowired
    private PersonalCollectionEssayMapper personalCollectionEssayMapper;

    @Override
    public List<Essay> showRelativeEassy(int relicId) {
        return essayMapper.showRelativeEassy(relicId);
    }

    @Override
    public Essay queryEssayById(int essayId) {
        return essayMapper.queryEssayById(essayId);
    }

    @Override
    public int insertEssay(Essay essay) {
        //获取id
        essay.setEssayId(1);
        if(essayMapper.countEssayNum()!=0){
            essay.setEssayId(essayMapper.maxEssayId()+1);
        }

        return essayMapper.insertEssay(essay);
    }

    @Override
    public  int updateEssay(Essay essay) {
        return essayMapper.updateEssay(essay);
    }

    @Override
    public int deleteEassy(int essayId) {
        //删除推荐表
        recommendMapper.deleteRecommend(essayId,"文章");
        //删除收藏
        personalCollectionEssayMapper.deleteCollectionBatch(essayId);
        return essayMapper.deleteEassy(essayId);
    }

    @Override
    public int updateEssayViewsNum(int essayId) {
        return recommendMapper.updateViewsNum(essayId,"文章");
    }

    @Override
    public List<Essay> queryEssayByUserId(int userId) {
        return essayMapper.queryEssayByUserId(userId);
    }

    @Override
    public List<Essay> queryEssay(String title) {
        return essayMapper.queryEssay(title);
    }

}
