package com.dcr.biz.impl;

import com.dcr.biz.ICreativeWorkshopBiz;
import com.dcr.entity.CreativeWorkshopSearch;
import com.dcr.entity.DiyCreation;
import com.dcr.entity.PersonalCollectionRelic;
import com.dcr.entity.Relic;
import com.dcr.mapper.CreativeWorkshopMapper;
import com.dcr.mapper.DiyCreationMapper;
import com.dcr.mapper.RecommendMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class CreativeWorkshopBiz implements ICreativeWorkshopBiz {
    @Autowired
    CreativeWorkshopMapper creativeWorkshopMapper;
    @Autowired
    DiyCreationMapper diyCreationMapper;
    @Autowired
    RecommendMapper recommendMapper;

    @Override
    public List<Relic> findwork(CreativeWorkshopSearch creativeWorkshopSearch) {
        List<Relic> list=creativeWorkshopMapper.findwork(creativeWorkshopSearch);
        List result=new ArrayList<Relic>();
        if(creativeWorkshopSearch.getSearch()!=null){
            String str=creativeWorkshopSearch.getSearch();
            str=str.trim();
            Pattern pattern = Pattern.compile(str, Pattern.CASE_INSENSITIVE);
            for(int i = 0 ; i<list.size(); i++) {
                //模糊匹配、大小写均可
                Matcher matcher = pattern.matcher(list.get(i).getRelicName());
                Matcher matcher1 = pattern.matcher(list.get(i).getRelicTime());
                Matcher matcher2 = pattern.matcher(list.get(i).getRelicType());
                Matcher matcher3 = pattern.matcher(list.get(i).getIntroduce());
                if (matcher.find()||matcher1.find()||matcher2.find()||matcher3.find()) {
                    result.add(list.get(i));
                }
            }
            return result;
        }else{
            return list;
        }
    }

    @Override
    //lsy修改
    public int addshow(DiyCreation diyCreation) {
        diyCreation.setShowId(1);
        if(creativeWorkshopMapper.countDiyNum()!=0){
            diyCreation.setShowId(creativeWorkshopMapper.maxEssayId()+1);
        }
        return creativeWorkshopMapper.addshow(diyCreation);
    }


    @Override
    public int addcollection(PersonalCollectionRelic personalCollectionRelic) {
//        Date date=new Date();
//        personalCollectionRelic.setCollectionTime(date);
        Timestamp collectionTime=new Timestamp(System.currentTimeMillis());
        personalCollectionRelic.setCollectionTime(collectionTime);
        creativeWorkshopMapper.addcollection(personalCollectionRelic);
        return recommendMapper.updateRelicCollectionNum(personalCollectionRelic.getRelicId(),"藏品");
    }

}
