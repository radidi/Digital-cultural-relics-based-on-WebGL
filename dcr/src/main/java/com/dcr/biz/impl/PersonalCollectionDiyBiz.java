package com.dcr.biz.impl;

import com.dcr.biz.IPersonalCollectionDiyBiz;
import com.dcr.entity.PersonalCollectionDiy;
import com.dcr.mapper.DiyCreationMapper;
import com.dcr.mapper.PersonalCollectionDiyMapper;
import com.dcr.mapper.RecommendMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class PersonalCollectionDiyBiz implements IPersonalCollectionDiyBiz {
    @Autowired
    private PersonalCollectionDiyMapper personalCollectionDiyMapper;
    @Autowired
    private RecommendMapper recommendMapper;
    @Autowired
    private DiyCreationMapper diyCreationMapper;
    @Override
    public List<PersonalCollectionDiy> showCollectionList(int userId) {
        return personalCollectionDiyMapper.showCollectionList(userId);
    }

    @Override
    public PersonalCollectionDiy showSelectedCollection(int userId, int showId) {//其实不需要userId？
        return personalCollectionDiyMapper.showSelectedCollection(userId,showId);
    }

    @Override
    public int sumCollection(int userId) {

        return  personalCollectionDiyMapper.sumCollection(userId);
    }

    @Override
    public int collect(PersonalCollectionDiy PCD) {
        Timestamp collectionTime=new Timestamp(System.currentTimeMillis());
        PCD.setCollectionTime(collectionTime);
        personalCollectionDiyMapper.collect(PCD);
        recommendMapper.updateRelicCollectionNum(PCD.getShowId(),"diy");
//        diyCreationMapper.updateCollectionNum(PCD.getShowId());
        return 0;
    }

    @Override
    public int deleteCollection(int userId, int showId) {
        personalCollectionDiyMapper.deleteCollection(userId,showId);
        recommendMapper.reduceCollectionNum(showId,"diy");
        return 0;
    }


    @Override
    public List<PersonalCollectionDiy> queryCollections(int userId, String keyword, String startTime, String endTime) {
        return personalCollectionDiyMapper.queryCollections(userId, keyword, startTime, endTime);
    }

    @Override
    public int deleteCollectionBatch(int showId) {
        return personalCollectionDiyMapper.deleteCollectionBatch(showId);
    }

    @Override
    public int whetherCollected(int showId, int userId) {
        if(personalCollectionDiyMapper.whetherCollected(showId,userId)==null){
            return 0;
        }else {
            return 1;
        }
    }

    @Override
    public int deleteBatchByRelicId(int relicId) {
        return personalCollectionDiyMapper.deleteBatchByRelicId(relicId);
    }
}
