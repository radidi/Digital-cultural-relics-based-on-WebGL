package com.dcr.biz.impl;

import com.dcr.biz.IDiyCreationBiz;
import com.dcr.entity.DiyCreation;
import com.dcr.entity.DiyCreationSearch;
import com.dcr.entity.PersonalCollectionDiy;
import com.dcr.entity.PersonalCollectionRelic;
import com.dcr.mapper.DiyCreationMapper;
import com.dcr.mapper.PersonalCollectionDiyMapper;
import com.dcr.mapper.RecommendMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class DiyCreationBiz implements IDiyCreationBiz {
    @Autowired
    private DiyCreationMapper diyCreationMapper;
    @Autowired
    private RecommendMapper recommendMapper;
    @Autowired
    private PersonalCollectionDiyMapper personalCollectionDiyMapper;

//-------dy↓
    @Override
    public List<DiyCreation> showPersonalCreation(int userId) {
        return diyCreationMapper.showPersonalCreation(userId);
    }

    @Override
    public DiyCreation showSelectedCreation(int showId) {
        return diyCreationMapper.showSelectedCreation(showId);
    }

    @Override
    public int sumCreation(int userId) {
        return diyCreationMapper.sumCreation(userId);
    }

    @Override
    public int updateCreation(DiyCreation diyCreation) {
        return diyCreationMapper.updateCreation(diyCreation);
    }

//-------dy↑


//lwj
    @Override
    public int deletework(int showId) {
        //lsy
        recommendMapper.deleteRecommend(showId,"diy");
        personalCollectionDiyMapper.deleteCollectionBatch(showId);
        return diyCreationMapper.deletework(showId);
    }


    @Override
    public List<DiyCreationSearch> findwork(DiyCreationSearch diyCreationSearch) {
        List<DiyCreationSearch> list=diyCreationMapper.findwork(diyCreationSearch);
        if(diyCreationSearch.getSearch()!=null){
            List result=new ArrayList<DiyCreationSearch>();
            String str=diyCreationSearch.getSearch();
            str=str.trim();
            Pattern pattern = Pattern.compile(str, Pattern.CASE_INSENSITIVE);
            for(int i = 0 ; i<list.size(); i++) {
                //模糊匹配、大小写均可
                Matcher matcher = pattern.matcher(list.get(i).getName());
                Matcher matcher1 = pattern.matcher(list.get(i).getRelicTime());
                Matcher matcher2 = pattern.matcher(list.get(i).getRelicType());
                Matcher matcher3 = pattern.matcher(list.get(i).getIntroduce());
                Matcher matcher4 = pattern.matcher(list.get(i).getRelicName());
                if (matcher.find()||matcher1.find()||matcher2.find()||matcher3.find()||matcher4.find()) {
                    result.add(list.get(i));
                }
            }
                return result;
         }else{

            return list;
                 }
        }

    @Override
    public int addDiyWork(PersonalCollectionDiy personalCollectionDiy) {
//        Date date=new Date();
//        personalCollectionDiy.setCollectionTime(date);
        Timestamp collectionTime=new Timestamp(System.currentTimeMillis());
        personalCollectionDiy.setCollectionTime(collectionTime);
        diyCreationMapper.addDiyWork(personalCollectionDiy);
        return diyCreationMapper.updateCollectionNum(personalCollectionDiy.getShowId());
    }

    @Override
    public DiyCreation findDiyWorkById(int showId) {
        return diyCreationMapper.findDiyWorkById(showId);
    }


}
