package com.dcr.biz.impl;

import com.dcr.biz.IPersonalCenterBiz;
import com.dcr.entity.DiyCreation;
import com.dcr.entity.PersonalCenter;
import com.dcr.mapper.PersonalCenterMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonalCenterBiz implements IPersonalCenterBiz {
    @Autowired
    private PersonalCenterMapper personalCenterMapper;
    @Override
    public PersonalCenter login(String username, String password) {

         PersonalCenter pc=new PersonalCenter();
         pc.setUsername(username);
         pc.setPassword(password);
         pc=personalCenterMapper.login(pc);

     return pc;
    }

    @Override
    public int register(String username, String password) {

            PersonalCenter pc = new PersonalCenter();
            pc.setUsername(username);
            pc.setPassword(password);
            if(personalCenterMapper.queryUserByUsername(username) != null ){
                System.out.println("账号已存在");
                return -1;
            }
            else {
                personalCenterMapper.register(pc);
                return 0;
            }


    }

    @Override
    public PersonalCenter showData(int userId) {
        return  personalCenterMapper.getPersonalInfoById(userId);

    }

    @Override
    public PersonalCenter queryUserByUsername(String username) {
        return personalCenterMapper.queryUserByUsername(username);
    }

    @Override
    public int updatePassword(int userId,String newPass) {
        PersonalCenter pc=new PersonalCenter();
        String password=newPass;
        pc.setPassword(password);
        pc.setUserId(userId);
        personalCenterMapper.updatePassword(pc);
        return 0;
    }

    @Override
    public String editPasswordByOld(int userId,String oldPass, String newPass) {
        PersonalCenter pc=personalCenterMapper.getPersonalInfoById(userId);
        if (newPass != null && !(newPass.isEmpty()) && (oldPass.isEmpty() || !(oldPass.equals(pc.getPassword())))) {
            return "原密码错误！";
        }
        else {
            pc.setPassword(newPass);
            personalCenterMapper.editPasswordByOld(pc);
            return "success";
        }

    }




    @Override
    public int changeInformation(PersonalCenter pc) {
        personalCenterMapper.changeInformation(pc);
        return 0;
    }

    @Override
    public List<Integer> getAllUsersId() {
        return personalCenterMapper.getAllUsersId();
    }

    @Override
    public List<PersonalCenter> getAllOrdinaryUsers() {
        return personalCenterMapper.getAllOrdinaryUsers();
    }


}
