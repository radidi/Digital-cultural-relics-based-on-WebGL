package com.dcr.biz.impl;

import com.dcr.biz.IRelicCommentWholeBiz;
import com.dcr.entity.RelicComment;
import com.dcr.entity.RelicCommentWhole;
import com.dcr.mapper.RelicCommentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class RelicCommentWholeBiz implements IRelicCommentWholeBiz {
    @Autowired
    private RelicCommentMapper relicCommentMapper;

    @Override
    public List<RelicCommentWhole> queryCommentWhole(int relicId) {
        List<RelicComment> relicComments = relicCommentMapper.queryCommentByRelic(relicId);
        List<RelicCommentWhole> relicCommentWholes = new LinkedList<>();

        for(RelicComment relicComment:relicComments){
            RelicCommentWhole relicCommentWhole = new RelicCommentWhole();
            relicCommentWhole.setRelicComment(relicComment);
            relicCommentWhole.setRelicComments(relicCommentMapper.queryComment(relicComment.getCommentId()));
            relicCommentWholes.add(relicCommentWhole);
        }
        return relicCommentWholes;
    }
}
