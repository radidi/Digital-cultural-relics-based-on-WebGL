package com.dcr.biz.impl;

import com.dcr.biz.IPersonalSecurityBiz;
import com.dcr.entity.PersonalSecurity;
import com.dcr.mapper.PersonalSecurityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class PersonalSecurityBiz implements IPersonalSecurityBiz {
    @Autowired
    private PersonalSecurityMapper personalSecurityMapper;
//    @Override
//    public int findOrUpdatePassword(String username) {
//
//        return 0;
//    }

    @Override
    public List<PersonalSecurity> getAllQuestions(String username) {
        return personalSecurityMapper.getAllQuestions(username);
    }
//
//    @Override
//    public List<Map<String, String>> getAllQuestions(String username) {
//        return personalSecurityMapper.getAllQuestions(username);
//    }

    @Override
    public int addQuestion(int userId, int questionId, String question, String answer) {
        PersonalSecurity ps=new PersonalSecurity();
        ps.setUserId(userId);
        ps.setQuestionId(questionId);
        ps.setQuestion(question);
        ps.setAnswer(answer);

        personalSecurityMapper.addQuestion(ps);
        return 0;
    }

    @Override
    public int editQuestion(int userId, int questionId, String question, String answer) {
        PersonalSecurity ps=new PersonalSecurity();
        ps.setUserId(userId);
        ps.setQuestionId(questionId);
        ps.setQuestion(question);
        ps.setAnswer(answer);

        personalSecurityMapper.editQuestion(ps);
        return 0;
    }

    @Override
    public int deleteQuestion(int userId, int questionId) {
        personalSecurityMapper.deleteQuestion(userId,questionId);
        return 0;
    }

    @Override
    public int getMaxQuestionId(int userId) {
        return personalSecurityMapper.getMaxQuestionId(userId);
    }

    @Override
    public List<PersonalSecurity> queryByUserId(int userId) {
        return personalSecurityMapper.queryByUserId(userId);
    }
}
