package com.dcr.biz.impl;

import com.dcr.biz.INewsBiz;
import com.dcr.entity.News;
import com.dcr.entity.PersonalCenter;
import com.dcr.entity.RelicComment;
import com.dcr.mapper.NewsMapper;
import com.dcr.mapper.PersonalCenterMapper;
import com.dcr.mapper.RelicCommentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NewsBiz implements INewsBiz {
    @Autowired
    private NewsMapper newsMapper;
    @Autowired
    private PersonalCenterMapper personalCenterMapper;
    @Autowired
    private RelicCommentMapper relicCommentMapper;

    @Override
    public List<News> showSentNews(int senderId) {
        return newsMapper.showSentNews(senderId);
    }

    @Override
    public List<News> showReceivedNews(int recipientId) {
        return newsMapper.showReceivedNews(recipientId);
    }

    @Override
    public List<News> showReceivedAdminNews( int recipientId) {
        return newsMapper.showReceivedAdminNews(recipientId);
    }

    @Override
    public int sumReceivedAdminNews(int recipientId) {
        return newsMapper.sumReceivedAdminNews(recipientId);
    }

    @Override
    public List<News> showReceivedUserNews( int recipientId) {
        return newsMapper.showReceivedUserNews(recipientId);
    }

    @Override
    public int sumReceivedUserNews(int recipientId) {
        return newsMapper.sumReceivedUserNews(recipientId);
    }

    @Override
    public News showNewsDetail(News news) {
        updateNewsStatus(news.getNewsId());
//        if (news.getStatus().equals("未读")){
//            newsMapper.updateNewsStatus(news);
//
//        }
        return newsMapper.showNewsDetail(news);
    }

    @Override
    public int updateNewsStatus(int newsId) {
        News news=newsMapper.queryByNewsId(newsId);
        if (news.getStatus().equals("未读")){
            news.setStatus("已读");
            newsMapper.updateNewsStatus(news);
        }
        return 0;
    }

    @Override
    public News queryByNewsId(int newsId) {
        return newsMapper.queryByNewsId(newsId);
    }

    @Override
    public int sumSentNews(int senderId) {
        return newsMapper.sumSentNews(senderId);
    }

    @Override
    public int sumReceivedNews(int recipientId) {
        return newsMapper.sumReceivedNews(recipientId);
    }

    @Override
    public int sumUnreadNews(int recipientId) {
        return newsMapper.sumUnreadNews(recipientId);
    }

    @Override
    public int sumUnreadAdminNews( int recipientId) {
        return newsMapper.sumUnreadAdminNews(recipientId);
    }

    @Override
    public int sumUnreadUserNews(int recipientId) {
        return newsMapper.sumUnreadUserNews(recipientId);
    }

    @Override
    public List<News> showUnreadAdminNews(int recipientId) {
        return newsMapper.showUnreadAdminNews(recipientId);
    }

    @Override
    public List<News> showUnreadUserNews(int recipientId) {
        return newsMapper.showUnreadUserNews(recipientId);
    }

    @Override
    public int sumReadNews(int recipientId) {
        return newsMapper.sumReadNews(recipientId);
    }

    @Override
    public int sumReadAdminNews(int recipientId) {
        return newsMapper.sumReadAdminNews(recipientId);
    }

    @Override
    public int sumReadUserNews(int recipientId) {
        return newsMapper.sumReadUserNews(recipientId);
    }

    @Override
    public List<News> showReadAdminNews(int recipientId) {
        return newsMapper.showReadAdminNews(recipientId);
    }

    @Override
    public List<News> showReadUserNews(int recipientId) {
        return newsMapper.showReadUserNews(recipientId);
    }

    @Override
    public int deleteSelectedNews(int newsId) {
        return newsMapper.deleteSelectedNews(newsId);
    }

    @Override
    public int deleteBatchNews(List<Integer> ids) {
        return newsMapper.deleteBatchNews(ids);
    }

    @Override
    public int sendNews(News news) {
        return newsMapper.sendNews(news);
    }

    @Override
    public int masterSendNewsBatch(List<News> newsList) {
        return newsMapper.masterSendNewsBatch(newsList);
    }

    @Override
    public int masterSendNewsToAll(List<News> newsList) {
        return newsMapper.masterSendNewsBatch(newsList);
    }

    @Override
    public int insertComment(RelicComment relicComment) {
        int replyId=relicComment.getReplyId();
        if (replyId!=0){
            News news=new News();
            int senderId=relicComment.getUserId();
            String senderName=personalCenterMapper.getPersonalInfoById(senderId).getUsername();
            String content=relicComment.getContent();
            content=senderName+" 回复了你:"+content;
            news.setContent(content);
            news.setTime(relicComment.getTime());
            news.setStatus("未读");
            news.setSenderId(senderId);
            news.setType("0");
            int recipientId=relicCommentMapper.queryUserIdByReplyId(replyId);
            news.setRecipientId(recipientId);
            return newsMapper.sendNews(news);
        }else {
            return 1;
        }

    }

    @Override
    public List<News> showHistory(int senderId, int recipientId) {
        return newsMapper.showHistory(senderId,recipientId);
    }

    @Override
    public List<PersonalCenter> historyUsers(int senderId) {
        return newsMapper.historyUsers(senderId);
    }

}
