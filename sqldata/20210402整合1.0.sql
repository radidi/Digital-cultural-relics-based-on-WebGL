/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 50719
 Source Host           : localhost:3306
 Source Schema         : dcr

 Target Server Type    : MySQL
 Target Server Version : 50719
 File Encoding         : 65001

 Date: 02/04/2021 20:22:35
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for creative_workshop
-- ----------------------------
DROP TABLE IF EXISTS `creative_workshop`;
CREATE TABLE `creative_workshop`  (
  `relic_id` int(20) NOT NULL AUTO_INCREMENT COMMENT '可进行创作展品id',
  PRIMARY KEY (`relic_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '创意工坊表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of creative_workshop
-- ----------------------------
INSERT INTO `creative_workshop` VALUES (1);

-- ----------------------------
-- Table structure for diy_creation
-- ----------------------------
DROP TABLE IF EXISTS `diy_creation`;
CREATE TABLE `diy_creation`  (
  `show_id` int(20) NOT NULL COMMENT '创作品id号',
  `relic_id` int(20) NULL DEFAULT NULL COMMENT '创作品的原型id',
  `user_id` int(20) NULL DEFAULT NULL COMMENT '创作者id',
  `show_model_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创作品路劲',
  `photo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `diy_collection_num` int(20) NULL DEFAULT 0,
  `diy_views_num` int(20) NULL DEFAULT 0,
  PRIMARY KEY (`show_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '展示栏' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of diy_creation
-- ----------------------------
INSERT INTO `diy_creation` VALUES (1, 1, 1, 'ceshi.jpg', 'ceshi.jpg', '创意1号', 4, 0);
INSERT INTO `diy_creation` VALUES (2, 1, 1, 'ceshi.jpg', 'ceshi.jpg', '创意2号', 0, 0);
INSERT INTO `diy_creation` VALUES (3, 1, 1, 'ceshi.jpg', 'ceshi.jpg', '创意3号', 0, 0);

-- ----------------------------
-- Table structure for essay
-- ----------------------------
DROP TABLE IF EXISTS `essay`;
CREATE TABLE `essay`  (
  `essay_id` int(20) NOT NULL AUTO_INCREMENT COMMENT '文章id',
  `title` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文章标题',
  `author` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '作者',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '内容',
  `relic_id` int(20) NULL DEFAULT NULL COMMENT '关联藏品id',
  `essay_collection_num` int(20) NULL DEFAULT 0 COMMENT '收藏数',
  `essay_views_num` int(20) NULL DEFAULT 0 COMMENT '浏览数',
  `user_id` int(20) NULL DEFAULT NULL COMMENT '作者id',
  `photo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '作者头像',
  PRIMARY KEY (`essay_id`) USING BTREE,
  INDEX `relic_essay1`(`relic_id`) USING BTREE,
  INDEX `essay_userid`(`user_id`) USING BTREE,
  INDEX `essay_photo_personalcenter`(`photo`) USING BTREE,
  CONSTRAINT `essay_ibfk_1` FOREIGN KEY (`relic_id`) REFERENCES `relic` (`relic_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `essay_userid` FOREIGN KEY (`user_id`) REFERENCES `personal_center` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of essay
-- ----------------------------
INSERT INTO `essay` VALUES (1, '北京故宫藏清代首饰之点翠海棠花纹头花赏析', '北京故宫网站', '点翠是我国古老的传统工艺，是用翠鸟的羽毛粘贴在金银制成的金属底托上而制成的。由于羽毛柔细，且色彩经久不褪，因此，虽历数百年的点翠饰品仍会明艳如初。这件点翠海棠花纹头花即用翠蓝色鸟羽粘贴而成，与白色玉石制成的海棠花交相衬托，于朴素中凸显高雅，于寻常中别具韵味，应是清代宫廷后妃喜爱的一款头饰。（资料来源：北京故宫网站）', 2, 0, 0, 1, 'user4.jpg');
INSERT INTO `essay` VALUES (3, '北京故宫藏清代首饰之点翠海棠花纹头花赏析', '北京故宫网站', '点翠是我国古老的传统工艺，是用翠鸟的羽毛粘贴在金银制成的金属底托上而制成的。由于羽毛柔细，且色彩经久不褪，因此，虽历数百年的点翠饰品仍会明艳如初。这件点翠海棠花纹头花即用翠蓝色鸟羽粘贴而成，与白色玉石制成的海棠花交相衬托，于朴素中凸显高雅，于寻常中别具韵味，应是清代宫廷后妃喜爱的一款头饰。（资料来源：北京故宫网站）', 2, 0, 0, 1, 'user2.jpg');
INSERT INTO `essay` VALUES (5, '看见文物1', '杨晓君\r\n', '海棠素有“国艳”之誉，大文豪苏东坡为之倾倒，有名句”只恐夜深花睡去，故烧高烛照红妆”，因此海棠有雅号“解语花”。\r\n\r\n点翠是我国古老的传统工艺，是用翠鸟的羽毛粘贴在金银制成的金属底托上而制成的，点翠工艺首饰有着独一无二的特点，每一件都是孤品，不会有相同的第二件出现。。由于羽毛柔细，且色彩经久不褪，因此，虽历数百年的点翠饰品仍会明艳如初。这件点翠海棠花纹头花即用翠蓝色鸟羽粘贴而成，与白色玉石制成的海棠花交相衬托，于朴素中凸显高雅，于寻常中别具韵味，应是清代宫廷后妃喜爱的一款头饰。', 2, 0, 1, 1, 'user1.jpg');
INSERT INTO `essay` VALUES (6, '看见文物2', '杨晓君', '翠鸟的数量稀少，更能显示佩戴者尊贵的身份以及地位，正所谓物以稀为贵，这正是点翠首饰深受后宫妃子、达官显贵女眷争相追捧的原因。近现代戏剧演员都以有一套点翠凤冠为傲，点翠凤冠标志着名角的身份。', 2, 0, 0, 1, 'user1.jpg');
INSERT INTO `essay` VALUES (7, '看见文物3', '杨晓君', '点翠工艺历史可谓源远流长，战国时期的古老故事《买椟还珠》中的楚国商人木匣“缀以珠玉，饰以玫瑰，辑以羽翠。”就是由点翠装饰的。而南朝梁第二任皇帝则正式给点翠工艺取名“谁家总角歧路阴，裁红点翠愁人心”。', 2, 0, 0, 1, 'user1.jpg');

-- ----------------------------
-- Table structure for home_recommend
-- ----------------------------
DROP TABLE IF EXISTS `home_recommend`;
CREATE TABLE `home_recommend`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '在表内的id',
  `origin_id` int(11) NOT NULL COMMENT '自己在原来表里的id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型',
  `status` int(11) NULL DEFAULT NULL COMMENT '0为不显示，1为显示',
  `times` int(11) NULL DEFAULT NULL COMMENT '推荐次数',
  `introduction` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '推荐介绍',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '预览图',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of home_recommend
-- ----------------------------
INSERT INTO `home_recommend` VALUES (1, 3, '赵之谦花卉册-桃花页', 'relic', 1, 1, '咸丰己未（1859）五月，赵之谦画了一册花卉册页，设色浓艳，构图别致，多用没骨画法画成，纸本设色，规格为纵22.9厘米、横31.9厘米，藏故宫博物院。是赵氏早期的精品。', 'user-index-fang.png');
INSERT INTO `home_recommend` VALUES (2, 2, '御瓷新见', 'essay', 1, 1, '故宫博物院-景德镇明代御窑遗址出土与故宫博物院藏传世瓷器对比展', 'yucixinjian.jpg');
INSERT INTO `home_recommend` VALUES (3, 4, '梅竹双清', 'essay', 1, 1, '苏州博物馆-苏州博物馆馆藏梅花与竹子题材特展', 'meizhushuangqing.jpg');

-- ----------------------------
-- Table structure for news
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news`  (
  `sender_id` int(20) NULL DEFAULT NULL COMMENT '消息发送者',
  `recipient_id` int(20) NULL DEFAULT NULL COMMENT '消息接受者',
  `news_id` int(20) NOT NULL AUTO_INCREMENT COMMENT '会话id',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '会话内容',
  `time` datetime(0) NULL DEFAULT NULL COMMENT '对话时间',
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '未读' COMMENT '状态（已读、未读）',
  `type` enum('0','1') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '判断是否是系统消息（管理员发给所有用户的通知），1：是，0：不是',
  PRIMARY KEY (`news_id`) USING BTREE,
  INDEX `news_check_user`(`sender_id`) USING BTREE,
  INDEX `recipient_check`(`recipient_id`) USING BTREE,
  CONSTRAINT `news_ibfk_1` FOREIGN KEY (`recipient_id`) REFERENCES `personal_center` (`user_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `news_ibfk_2` FOREIGN KEY (`sender_id`) REFERENCES `personal_center` (`user_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 36 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '消息' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of news
-- ----------------------------
INSERT INTO `news` VALUES (2, 1, 1, '咪西咪西', '2020-07-29 10:23:29', '已读', '0');
INSERT INTO `news` VALUES (1, 2, 2, '摩西摩西', '2020-07-29 10:23:43', '已读', '0');
INSERT INTO `news` VALUES (2, 1, 3, '不放过一针一线！', '2020-07-29 10:24:11', '未读', '0');
INSERT INTO `news` VALUES (1, 2, 4, '了解desu！', NULL, '未读', '0');
INSERT INTO `news` VALUES (1, 2, 5, '你滴，大大滴干活', NULL, '已读', '0');
INSERT INTO `news` VALUES (3, 4, 6, 'I‘m your father', NULL, '未读', '0');
INSERT INTO `news` VALUES (4, 3, 7, 'noooooooooooooooooooooooooooooo', NULL, '未读', '0');
INSERT INTO `news` VALUES (3, 5, 8, 'i hate you', NULL, '未读', '0');
INSERT INTO `news` VALUES (5, 3, 9, 'i love you', NULL, '未读', '0');
INSERT INTO `news` VALUES (3, 1, 10, '空吧哇，瓦塔西阿纳鸡desu', NULL, '未读', '0');
INSERT INTO `news` VALUES (4, 1, 11, '大创测试', '2020-08-06 17:00:24', '未读', '0');
INSERT INTO `news` VALUES (3, 2, 12, '安走天 回复了你:你的想法很危险……', '2020-08-29 15:10:13', '未读', '0');
INSERT INTO `news` VALUES (1, 2, 13, 'user 回复了你:？？？啥b？', '2020-08-29 15:10:40', '未读', '0');
INSERT INTO `news` VALUES (2, 4, 14, '太君 回复了你:青玉童子：你再骂？', '2020-08-29 15:12:05', '未读', '0');
INSERT INTO `news` VALUES (3, 1, 15, '安走天 回复了你:哥你回复不要单独发一楼ok？占用公共资源搞你们饭圈那套真恶心。（pop子yyds！）', '2020-08-29 15:14:30', '未读', '0');
INSERT INTO `news` VALUES (5, 5, 16, '人品王 回复了你:听说改名叫屁屁美了，连个通知都没有，真是不把我们粉丝当回事。', '2020-08-29 15:15:36', '未读', '0');
INSERT INTO `news` VALUES (4, 5, 17, '卢走天 回复了你:天哪，就因为狗老屁给网站捐钱？无78语。', '2020-08-29 15:16:32', '未读', '0');
INSERT INTO `news` VALUES (3, 1, 28, '国庆开业大酬宾，活动期间在商城购物即可获得”你号没了“buff七天！', '2020-08-29 16:22:55', '未读', '1');
INSERT INTO `news` VALUES (3, 2, 29, '国庆开业大酬宾，活动期间在商城购物即可获得”你号没了“buff七天！', '2020-08-29 16:22:55', '未读', '1');
INSERT INTO `news` VALUES (3, 3, 30, '国庆开业大酬宾，活动期间在商城购物即可获得”你号没了“buff七天！', '2020-08-29 16:22:55', '未读', '1');
INSERT INTO `news` VALUES (3, 4, 31, '国庆开业大酬宾，活动期间在商城购物即可获得”你号没了“buff七天！', '2020-08-29 16:22:55', '未读', '1');
INSERT INTO `news` VALUES (3, 5, 32, '国庆开业大酬宾，活动期间在商城购物即可获得”你号没了“buff七天！', '2020-08-29 16:22:55', '未读', '1');
INSERT INTO `news` VALUES (3, 4, 33, '你号没了', '2020-08-29 16:24:40', '未读', '0');
INSERT INTO `news` VALUES (3, 5, 34, '你号没了', '2020-08-29 16:24:40', '未读', '0');
INSERT INTO `news` VALUES (2, 7, 35, 'T T', '2020-08-29 17:26:47', '未读', '0');

-- ----------------------------
-- Table structure for personal_center
-- ----------------------------
DROP TABLE IF EXISTS `personal_center`;
CREATE TABLE `personal_center`  (
  `user_id` int(20) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户昵称',
  `role` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '管理员/用户',
  `password` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `photo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `download` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '下载路径',
  PRIMARY KEY (`user_id`) USING BTREE,
  INDEX `photo`(`photo`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of personal_center
-- ----------------------------
INSERT INTO `personal_center` VALUES (1, 'user', '普通用户', '123456', 'user1.jpg', NULL);
INSERT INTO `personal_center` VALUES (2, '太君', '管理员', 'miximixi', 'user2.jpg', NULL);
INSERT INTO `personal_center` VALUES (3, '安走天', '管理员', 'Anakin', 'user3.jpg', NULL);
INSERT INTO `personal_center` VALUES (4, '卢走天', '普通用户', 'babyluke', 'user4.jpg', NULL);
INSERT INTO `personal_center` VALUES (5, '人品王', '管理员', 'obi-wan', 'usr5.jpg', NULL);
INSERT INTO `personal_center` VALUES (6, 'jsfspts', '普通用户', 'BeaTleS', NULL, NULL);
INSERT INTO `personal_center` VALUES (7, 'Twice', '普通用户', 'WhatIsLove', NULL, NULL);
INSERT INTO `personal_center` VALUES (8, 'lsy', '普通用户', '641lsy', NULL, NULL);
INSERT INTO `personal_center` VALUES (9, 'zyh', '普通用户', 'zyhdsb', NULL, NULL);
INSERT INTO `personal_center` VALUES (10, 'cpy', '普通用户', 'cpycqy', NULL, NULL);
INSERT INTO `personal_center` VALUES (11, 'dy', '管理员', 'handsome', NULL, NULL);

-- ----------------------------
-- Table structure for personal_collection_diy
-- ----------------------------
DROP TABLE IF EXISTS `personal_collection_diy`;
CREATE TABLE `personal_collection_diy`  (
  `user_id` int(20) NOT NULL COMMENT '用户id',
  `show_id` int(20) NOT NULL COMMENT '创作品id',
  `collection_time` datetime(0) NULL DEFAULT NULL COMMENT '收藏时间',
  PRIMARY KEY (`user_id`, `show_id`) USING BTREE,
  INDEX `pc_relic_check`(`show_id`) USING BTREE,
  CONSTRAINT `personal_collection_diy_ibfk_1` FOREIGN KEY (`show_id`) REFERENCES `diy_creation` (`show_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `personal_collection_diy_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `personal_center` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户收藏品' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of personal_collection_diy
-- ----------------------------
INSERT INTO `personal_collection_diy` VALUES (1, 1, '2020-09-04 17:30:34');
INSERT INTO `personal_collection_diy` VALUES (2, 1, '2020-09-04 17:31:18');
INSERT INTO `personal_collection_diy` VALUES (3, 1, '2020-09-04 17:36:49');
INSERT INTO `personal_collection_diy` VALUES (4, 1, '2020-09-04 17:39:30');

-- ----------------------------
-- Table structure for personal_collection_essay
-- ----------------------------
DROP TABLE IF EXISTS `personal_collection_essay`;
CREATE TABLE `personal_collection_essay`  (
  `user_id` int(20) NOT NULL COMMENT '用户id',
  `essay_id` int(20) NOT NULL COMMENT '文章id',
  `collection_time` datetime(0) NULL DEFAULT NULL COMMENT '收藏时间',
  PRIMARY KEY (`user_id`, `essay_id`) USING BTREE,
  INDEX `pce_essay_check`(`essay_id`) USING BTREE,
  CONSTRAINT `personal_collection_essay_ibfk_1` FOREIGN KEY (`essay_id`) REFERENCES `essay` (`essay_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `personal_collection_essay_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `personal_center` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of personal_collection_essay
-- ----------------------------
INSERT INTO `personal_collection_essay` VALUES (1, 1, '2020-09-04 17:33:46');

-- ----------------------------
-- Table structure for personal_collection_relic
-- ----------------------------
DROP TABLE IF EXISTS `personal_collection_relic`;
CREATE TABLE `personal_collection_relic`  (
  `user_id` int(20) NOT NULL COMMENT '用户id',
  `relic_id` int(20) NOT NULL COMMENT '藏品id',
  `collection_time` datetime(0) NULL DEFAULT NULL COMMENT '收藏时间',
  PRIMARY KEY (`user_id`, `relic_id`) USING BTREE,
  INDEX `pc_relic_check`(`relic_id`) USING BTREE,
  CONSTRAINT `personal_collection_relic_ibfk_1` FOREIGN KEY (`relic_id`) REFERENCES `relic` (`relic_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `personal_collection_relic_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `personal_center` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户收藏品' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of personal_collection_relic
-- ----------------------------
INSERT INTO `personal_collection_relic` VALUES (1, 1, '2020-09-04 17:34:46');
INSERT INTO `personal_collection_relic` VALUES (1, 5, '2020-09-04 17:34:08');
INSERT INTO `personal_collection_relic` VALUES (4, 1, '2020-09-04 17:38:52');

-- ----------------------------
-- Table structure for personal_security
-- ----------------------------
DROP TABLE IF EXISTS `personal_security`;
CREATE TABLE `personal_security`  (
  `user_id` int(20) NOT NULL COMMENT '用户id',
  `question_id` int(20) NOT NULL COMMENT '问题id',
  `question` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密保问题',
  `answer` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`, `question_id`) USING BTREE,
  CONSTRAINT `personal_security_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `personal_center` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of personal_security
-- ----------------------------
INSERT INTO `personal_security` VALUES (1, 1, '罗普兰是女的吗？', '全世界没有女的了罗普兰也不是女的');

-- ----------------------------
-- Table structure for recommend
-- ----------------------------
DROP TABLE IF EXISTS `recommend`;
CREATE TABLE `recommend`  (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `hot_num` int(20) NULL DEFAULT 0 COMMENT '热度',
  `status` enum('0','1') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '显示状态',
  `type` enum('藏品','文章','diy') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '类型',
  PRIMARY KEY (`id`, `type`) USING BTREE,
  INDEX `hot_num`(`hot_num`) USING BTREE COMMENT '按热度排序'
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '推荐' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of recommend
-- ----------------------------
INSERT INTO `recommend` VALUES (1, 3, '1', '藏品');
INSERT INTO `recommend` VALUES (1, 1, '0', '文章');
INSERT INTO `recommend` VALUES (2, 6, '1', '藏品');
INSERT INTO `recommend` VALUES (3, 0, '1', '藏品');
INSERT INTO `recommend` VALUES (3, 0, '1', '文章');
INSERT INTO `recommend` VALUES (3, 2, '1', 'diy');
INSERT INTO `recommend` VALUES (4, 1, '0', '藏品');
INSERT INTO `recommend` VALUES (5, 1, '1', '藏品');
INSERT INTO `recommend` VALUES (5, 1, '1', '文章');
INSERT INTO `recommend` VALUES (6, 0, '1', '藏品');
INSERT INTO `recommend` VALUES (6, 0, '1', '文章');
INSERT INTO `recommend` VALUES (7, 0, '1', '藏品');
INSERT INTO `recommend` VALUES (7, 0, '1', '文章');

-- ----------------------------
-- Table structure for relic
-- ----------------------------
DROP TABLE IF EXISTS `relic`;
CREATE TABLE `relic`  (
  `relic_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '藏品名',
  `relic_id` int(20) NOT NULL AUTO_INCREMENT COMMENT '藏品id',
  `relic_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '藏品年代',
  `relic_type` enum('陶瓷器','青铜器','雕塑','漆器','书画','玉器','金银器','其他') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类',
  `relic_picture` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '藏品图片路径',
  `model` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '藏品模型路径',
  `introduce` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '藏品介绍',
  `relic_collection_num` int(20) NULL DEFAULT 0 COMMENT '收藏数',
  `relic_views_num` int(20) NULL DEFAULT 0 COMMENT '浏览数',
  PRIMARY KEY (`relic_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '藏品表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of relic
-- ----------------------------
INSERT INTO `relic` VALUES ('四灵八卦镜', 1, '清', '青铜器', '1.jpg', NULL, '四灵八卦镜，清代，铜质。八卦镜指的是文王八卦镜也叫太极八卦镜，在中国一直被称为风水吉祥物。据传，在距今七千年前的上古时代，伏羲氏观物取象，始作八卦。\r\n风水师认为镜子的化煞功能实属一等一，走在大街小巷中，很容易可以发现悬挂在屋檐墙壁上的八卦镜。在选择八卦镜时以用途选择，凸镜镇宅化煞，凹镜吸财纳福，太极八卦镜可扭转乾坤调节风水，作为传统八卦镜，以天然玉石材质为风水功效佳。', 3, 10);
INSERT INTO `relic` VALUES ('点翠海棠花纹头花', 2, '清', '其他', '2.jpg', NULL, '清宫旧藏，横22cm，纵20cm。头花用银镀金材质做底托，其上镶嵌点翠，用玉石做成海棠花形装饰。\r\n\r\n点翠是我国古老的传统工艺，是用翠鸟的羽毛粘贴在金银制成的金属底托上而制成的。由于羽毛柔细，且色彩经久不褪，因此，虽历数百年的点翠饰品仍会明艳如初。这件点翠海棠花纹头花即用翠蓝色鸟羽粘贴而成，与白色玉石制成的海棠花交相衬托，于朴素中凸显高雅，于寻常中别具韵味，应是清代宫廷后妃喜爱的一款头饰。（资料来源：北京故宫网站）', 0, 7);
INSERT INTO `relic` VALUES ('五彩蝴蝶纹梅瓶', 3, '清康熙', '陶瓷器', '3.jpg', NULL, '五彩蝴蝶纹瓶制作于康熙年间，属宫廷陈设品，大方、美观、是清宫廷陈设品的代表。江西景德镇御窑厂烧制，宫廷陈设品。花蝶形象逼真，千姿百态，别有情趣。蝶上施彩丰富，有红、黄、缘、蓝、紫、褐、黑、金等多种颜色，色彩搭配协调，充分显示了康熙朝制瓷匠师们高超的工艺技巧。', 0, 0);
INSERT INTO `relic` VALUES ('洒蓝描金小棒槌瓶', 4, '清康熙', '陶瓷器', '4.jpg', NULL, '洒蓝釉描金小棒槌瓶，高25.5厘米、口径7.2厘米、足径7.3厘米，故宫博物院藏。瓶内部及口沿施白釉，外壁在白釉地上施洒蓝釉，腹部描金绘竹枝，伴以折枝菊花。画右残存描金楷书“东篱把酒对南山”诗句，落款“西樵”，钤描金篆字“木石居”印。瓶外底心白釉，无款。', 0, 1);
INSERT INTO `relic` VALUES ('桐荫仕女玉山', 5, '清乾隆', '玉器', '5.jpg', NULL, '此玉山白玉质，有黄褐色玉皮。以月亮门为界，把庭院分为前后两部分，洞门半掩，门外右侧站一女子手持灵芝，周围有假山、桐树；门内另一侧亦立一女子，手捧宝瓶，与外面的女子从门缝中对视，周围有芭蕉树、石凳、石桌和山石等。\r\n\r\n器底阴刻乾隆御制诗、文各一。\r\n\r\n诗云：\r\n\r\n相材取碗料，就质琢图形。\r\n\r\n剩水残山境，桐檐蕉轴庭。\r\n\r\n女郎相顾问，匠氏运心灵。\r\n\r\n义重无弃物，赢他泣楚廷。\r\n\r\n末署“乾隆癸巳新秋御题”及“乾”、“隆”印各一。文曰：“和阗贡玉，规其中作碗，吴工就余材琢成是图。既无弃物，且仍完璞玉。御识。”末有“太璞”印。\r\n\r\n本器从内容到风格皆仿油画《桐荫仕女图》而作，所用玉料实为雕碗后的弃物，但玉工巧为施艺，庭院幽幽，人物传神，人们似可听到两女子透过门缝的窃窃私语。剩料被加以利用，这种取其自然之形和自然之色传以生动之神的做法，正符合“势者，乘利而为制也”（《文心雕龙·定势》）。此器是清代圆雕玉器的代表作。', 1, 1);
INSERT INTO `relic` VALUES ('青玉童子骑象', 6, '唐', '玉器', '6.jpg', NULL, '高5.5厘米，长7.3厘米，厚2.8厘米。\r\n\r\n青玉有绺裂、絮斑。 圆雕一男子侧身坐于象背之上。象卧伏，神态温顺。人物头戴软帽，微后仰。着长袖衫。右腿搭于左腿之上，右手举起放于脑后，左手置于腿上，长袖下垂，作舞蹈之姿。\r\n\r\n与历代统治者一样，唐代帝王对来自域外之物总是充满好奇，而邻国亦乐于以特产进献，以获取丰厚的回馈。当时西域和东南亚进献的动物有马、狮子、象、豹、犀、鹦鹉等。这些动物被豢养在宫廷兽苑中，有专人照看，并有固定的食料供应。宫廷每有宴乐，或逢重大庆典、节日，往往以百戏杂乐助兴，五坊使引驯象、驯犀、驯狮、舞马入场，随音乐或拜或舞。唐人诗文对此多有吟咏。本品描绘的就是男性舞人在驯象背上挥袖表演的场景。舞马、驯狮作为装饰题材，在唐代金器、玉器上比较多见，唯驯象，目前仅见此一例。', 0, 1);
INSERT INTO `relic` VALUES ('白玉天鸡三耳罐', 7, '唐', '玉器', '7.jpg', NULL, '高6.4厘米，口径4.9厘米，腹径7.5厘米。\n\n罐由白玉制成，圆口，鼓腹，圈足，足壁 镂雕3个方形孔。器表等距饰有3天鸡，作展翅直立状， 圆雕头部，翅膀与爪 浮雕于器身，以阴线刻出羽毛纹理，线条简洁流畅。\n\n清代的天鸡外形特点为前有卷须，后有垂凤尾。此器上的天鸡图案造型还保留着较为原始的形象，除双翅着意刻画外，凤尾、卷须尚未出现。如此形状的唐代天鸡作品非常少见。', 0, 1);
INSERT INTO `relic` VALUES ('青铜牛尊', 8, '商', '青铜器', '8.jpg', NULL, '这件牺尊以牛为造型，整座尊共分为两个部分，以牛的头部、背部为一部分作为牛尊的盖子，而牛的四肢、腹部、颈部、臀部和尾部作为器物的主体部分即器身。器身大而盖小，盖子边缘呈曲线形依附并吻合于器身。器盖呈卧立的“S”形，“S”的两头分别被设计成两种动物的头部形象，位于水平线上较高的一端为牛头，牛的眼球园鼓突出，上有一凹痕以示眼珠，并以阴刻线条表现眼眶。牛的两耳为小椭圆形，横向外撇，两耳之上，在牛头的顶部，是一支厚重、敦实的牛角，牛角从中间一分为二，角尖对称分布，直指后方，牛角素面光洁，仅有几条简单的竖向划痕用来表示牛角的肌理。另一端，处于“S”形水平线上较低的一头为鱼头。', 0, 10);

-- ----------------------------
-- Table structure for relic_comment
-- ----------------------------
DROP TABLE IF EXISTS `relic_comment`;
CREATE TABLE `relic_comment`  (
  `comment_id` int(20) NOT NULL COMMENT '评论id',
  `relic_id` int(20) NULL DEFAULT NULL COMMENT '藏品id',
  `user_id` int(20) NULL DEFAULT NULL COMMENT '用户id',
  `likes_num` int(20) NULL DEFAULT 0 COMMENT '赞数',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '内容',
  `reply_id` int(20) NULL DEFAULT 0 COMMENT '回复的评论的id',
  `time` datetime(0) NULL DEFAULT NULL COMMENT '回复时间',
  PRIMARY KEY (`comment_id`) USING BTREE,
  INDEX `relic_relic_comment1`(`relic_id`) USING BTREE,
  INDEX `relic_comment_relic_comment`(`reply_id`) USING BTREE,
  INDEX `personal_center_relic_comment1`(`user_id`) USING BTREE,
  CONSTRAINT `relic_comment_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `personal_center` (`user_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `relic_comment_ibfk_2` FOREIGN KEY (`reply_id`) REFERENCES `relic_comment` (`comment_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `relic_comment_ibfk_3` FOREIGN KEY (`relic_id`) REFERENCES `relic` (`relic_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '藏品评价表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of relic_comment
-- ----------------------------
INSERT INTO `relic_comment` VALUES (0, NULL, NULL, 0, NULL, 0, NULL);
INSERT INTO `relic_comment` VALUES (1, 1, 1, 0, 'pop子真的太好看了！', 0, '2020-07-30 17:14:17');
INSERT INTO `relic_comment` VALUES (2, 2, 1, 0, '看到它的第一眼我就无法自拔，点翠是世界上最tm好看的东西。', 0, '2020-08-29 15:06:01');
INSERT INTO `relic_comment` VALUES (3, 2, 2, 0, '我奶奶家祖传的昝钗就是点翠凤凰纹，待会我把这个也偷回去。', 0, '2020-08-29 15:09:21');
INSERT INTO `relic_comment` VALUES (4, 2, 3, 0, '你的想法很危险……', 3, '2020-08-29 15:10:13');
INSERT INTO `relic_comment` VALUES (5, 2, 1, 0, '？？？啥b？', 3, '2020-08-29 15:10:40');
INSERT INTO `relic_comment` VALUES (6, 6, 4, 0, '哈哈哈这是什么，好好笑。', 0, '2020-08-29 15:11:36');
INSERT INTO `relic_comment` VALUES (7, 6, 2, 0, '青玉童子：你再骂？', 6, '2020-08-29 15:12:05');
INSERT INTO `relic_comment` VALUES (8, 6, 1, 0, '很灵性。', 0, '2020-08-29 15:12:33');
INSERT INTO `relic_comment` VALUES (9, 1, 2, 0, 'pop子yyds，谁同意谁反对？', 0, '2020-08-29 15:13:17');
INSERT INTO `relic_comment` VALUES (10, 1, 1, 0, '我同意，yyds！', 0, '2020-08-29 15:13:38');
INSERT INTO `relic_comment` VALUES (11, 1, 3, 0, '哥你回复不要单独发一楼ok？占用公共资源搞你们饭圈那套真恶心。（pop子yyds！）', 10, '2020-08-29 15:14:30');
INSERT INTO `relic_comment` VALUES (12, 1, 4, 0, '为什么没有pipi美，还没出土吗？', 0, '2020-08-29 15:14:57');
INSERT INTO `relic_comment` VALUES (13, 1, 5, 0, '听说改名叫屁屁美了，连个通知都没有，真是不把我们粉丝当回事。', 12, '2020-08-29 15:15:36');
INSERT INTO `relic_comment` VALUES (14, 1, 4, 0, '天哪，就因为狗老屁给网站捐钱？无78语。', 12, '2020-08-29 15:16:32');
INSERT INTO `relic_comment` VALUES (15, 8, 1, 0, '这是牛？不是很像牛……', 0, '2021-03-18 19:46:36');
INSERT INTO `relic_comment` VALUES (16, 8, 7, 0, '我也觉得哈哈哈！', 15, '2021-03-29 11:47:32');
INSERT INTO `relic_comment` VALUES (17, 8, 3, 0, '看过相关文献，也有研究认为这件牛尊不是牺尊而是觥，详见：施劲松：《长江流域青铜器研究》。', 0, '2021-03-30 22:49:08');
INSERT INTO `relic_comment` VALUES (18, 8, 4, 0, '学到了学到了！！！', 17, '2021-03-31 20:51:04');
INSERT INTO `relic_comment` VALUES (19, 8, 2, 0, '我觉得有点像猪^(*￣(oo)￣)^', 0, '2021-03-31 23:03:43');

-- ----------------------------
-- Procedure structure for increase_collection_num
-- ----------------------------
DROP PROCEDURE IF EXISTS `increase_collection_num`;
delimiter ;;
CREATE PROCEDURE `increase_collection_num`(IN `id` int,IN `type` VARCHAR(20))
BEGIN
DECLARE hotNum INT DEFAULT 0;

IF (type = '藏品') THEN
	UPDATE relic 
	SET relic_collection_num = relic_collection_num+1
	WHERE relic_id=id;
	
	SELECT relic_collection_num*0.5 + relic_views_num*0.5 INTO hotNum
	FROM relic WHERE relic_id=id;

ELSEIF (type = '文章') THEN
	UPDATE essay 
	SET essay_collection_num = essay_collection_num+1
	WHERE essay_id=id;
	
	SELECT essay_collection_num*0.5 + essay_views_num*0.5 INTO hotNum
	FROM essay WHERE essay_id=id;

ELSEIF (type='diy') THEN
	UPDATE diy_creation
	SET diy_collection_num = diy_collection_num+1
	WHERE show_id=id;
	
	SELECT diy_collection_num*0.5 + diy_views_num*0.5 INTO hotNum
	FROM diy_creation WHERE show_id=id;
END IF;

UPDATE recommend
SET hot_num=hotNum
WHERE recommend.id=id and recommend.type=type;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for increase_views_num
-- ----------------------------
DROP PROCEDURE IF EXISTS `increase_views_num`;
delimiter ;;
CREATE PROCEDURE `increase_views_num`(IN `id` int,IN `type` VARCHAR(20))
BEGIN
DECLARE hotNum INT DEFAULT 0;

IF (type = '藏品') THEN
	UPDATE relic 
	SET relic_views_num = relic_views_num+1
	WHERE relic_id=id;
	
	SELECT relic_collection_num*0.5 + relic_views_num*0.5 INTO hotNum
	FROM relic WHERE relic_id=id;

ELSEIF (type = '文章') THEN
	UPDATE essay 
	SET essay_views_num = essay_views_num+1
	WHERE essay_id=id;
	
	SELECT essay_collection_num*0.5 + essay_views_num*0.5 INTO hotNum
	FROM essay WHERE essay_id=id;

ELSEIF (type='diy') THEN
	UPDATE diy_creation
	SET diy_views_num = diy_views_num+1
	WHERE show_id=id;
	
	SELECT diy_collection_num*0.5 + diy_views_num*0.5 INTO hotNum
	FROM diy_creation WHERE show_id=id;
END IF;

UPDATE recommend
SET hot_num=hotNum
WHERE recommend.id=id and recommend.type=type;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for insert_diy
-- ----------------------------
DROP PROCEDURE IF EXISTS `insert_diy`;
delimiter ;;
CREATE PROCEDURE `insert_diy`(IN `showId` int,IN `relicId` int,IN `userId` int,IN `showModelPath` varchar(255),IN `photo` varchar(255),IN `name` varchar(255))
BEGIN
	SET @type='diy';
	insert into diy_creation(show_id,relic_id,user_id,show_model_path,photo,name)
        values (showId,relicId,userId,showModelPath,photo,name);
				
	INSERT INTO recommend(id,type) 
	VALUES(showId,@type);
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for insert_essay
-- ----------------------------
DROP PROCEDURE IF EXISTS `insert_essay`;
delimiter ;;
CREATE PROCEDURE `insert_essay`(IN `essayId` int,IN `title` varchar(40),`author` varchar(20),`content` varchar(255),`relicId` int,`userId` int)
BEGIN
-- 	DECLARE type VARCHAR(20);
 	SET @type='文章';
	
	INSERT INTO essay(essay_id,title,author,content,relic_id,user_id,photo)
	VALUES (essayId,title,author,content,relicId,userId,
		(SELECT personal_center.photo FROM personal_center 
		WHERE personal_center.user_id=userId
		));
	
	INSERT INTO recommend(id,type) 
	VALUES(essayId,@type);
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for insert_relic
-- ----------------------------
DROP PROCEDURE IF EXISTS `insert_relic`;
delimiter ;;
CREATE PROCEDURE `insert_relic`(IN `relicName` varchar(20),IN `relicId` int,IN `relicTime` varchar(20),IN `relicType` varchar(20),IN `relicPicture` varchar(50),IN `model` varchar(50),`introduce` varchar(255))
BEGIN
	SET @type='藏品';
	INSERT INTO relic(relic_name,relic_id,relic_time,relic_type,relic_picture,model,introduce)
	VALUES (relicName,relicId,relicTime,relicType,relicPicture,model,introduce); 
	
	INSERT INTO recommend(id,type) 
	VALUES(relicId,@type);
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for reduce_collection_num
-- ----------------------------
DROP PROCEDURE IF EXISTS `reduce_collection_num`;
delimiter ;;
CREATE PROCEDURE `reduce_collection_num`(IN `id` int,IN `type` VARCHAR(20))
BEGIN
DECLARE hotNum INT DEFAULT 0;

IF (type = '藏品') THEN
	UPDATE relic 
	SET relic_collection_num = relic_collection_num-1
	WHERE relic_id=id;
	
	SELECT relic_collection_num*0.5 + relic_views_num*0.5 INTO hotNum
	FROM relic WHERE relic_id=id;

ELSEIF (type = '文章') THEN
	UPDATE essay 
	SET essay_collection_num = essay_collection_num-1
	WHERE essay_id=id;
	
	SELECT essay_collection_num*0.5 + essay_views_num*0.5 INTO hotNum
	FROM essay WHERE essay_id=id;

ELSEIF (type='diy') THEN
	UPDATE diy_creation
	SET diy_collection_num = diy_collection_num-1
	WHERE show_id=id;
	
	SELECT diy_collection_num*0.5 + diy_views_num*0.5 INTO hotNum
	FROM diy_creation WHERE show_id=id;
END IF;

UPDATE recommend
SET hot_num=hotNum
WHERE recommend.id=id and recommend.type=type;

END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
